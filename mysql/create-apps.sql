CREATE TABLE Applications
( AppsID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  SwUrl VARCHAR(100) NOT NULL, 
  Description VARCHAR(254) NOT NULL, 
  SwBased VARCHAR(100) NULL, 
  SwLanguage VARCHAR(100) NULL, 
  DbType VARCHAR(150) NULL, 
  ServerName VARCHAR(254) NOT NULL, 
  OwnerID INT(11) NOT NULL, 
  CreatorID INT(11) NULL, 
  OperatorID INT(11) NULL, 
  TicketID VARCHAR(16) NULL, 
  StartDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  ExpirationDate TIMESTAMP NULL, 
  State ENUM('active', 'suspended', 'inDevelopment', 'inactive') NOT NULL,
  Documentation VARCHAR(254) NULL, 
  RDproject BIT(1) NOT NULL 
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ALTER TABLE Applications
--   ADD FOREIGN KEY (ServerName) REFERENCES vmcharacteristics (vmName);
--   ADD FOREIGN KEY (OwnerID) REFERENCES owners (ID);
--   ADD FOREIGN KEY (CreatorID) REFERENCES owners (ID);
--   ADD FOREIGN KEY (OperatorID) REFERENCES owners (ID);
-- COMMIT;