-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Φιλοξενητής: localhost
-- Χρόνος δημιουργίας: 04 Ιουν 2018 στις 11:25:27
-- Έκδοση διακομιστή: 5.7.22
-- Έκδοση PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `UCDC`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `Carriers`
--

CREATE TABLE `Carriers` (
  `ID` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CarrierName` varchar(254) NOT NULL DEFAULT 'Πανεπιστήμιο Κρήτης',
  `School` varchar(254) NOT NULL DEFAULT 'NULL',
  `Department` varchar(254) NOT NULL DEFAULT 'NULL',
  `Sector` varchar(254) NOT NULL DEFAULT 'NULL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `Carriers`
--

INSERT INTO `Carriers` (`ID`, `modificationDate`, `CarrierName`, `School`, `Department`, `Sector`) VALUES
(202555197, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Ειδικός Λογαριασμός', 'NULL', 'NULL'),
(957041537, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Ιατρική Σχολή', 'Τμήμα Ιατρικής', 'NULL'),
(746074467, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Κέντρο Υποδομών & Υπηρεσιών ΤΠΕ', 'Μονάδα Διαχείρισης Πληροφορίας', 'NULL'),
(239624860, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Κέντρο Υποδομών & Υπηρεσιών ΤΠΕ', 'Μονάδα Δικτύων & Επικοινωνιών', 'NULL'),
(875107655, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Κέντρο Υποδομών & Υπηρεσιών ΤΠΕ', 'Μονάδα ΠΣ Διοίκησης & Οικ. Διαχείρισης', 'NULL'),
(287593390, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Κέντρο Υποδομών & Υπηρεσιών ΤΠΕ', 'Μονάδα Υπολογιστικών Υποδομών', 'NULL'),
(243357720, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Πρυτανεία', 'NULL', 'NULL'),
(438694963, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Σχολή Επιστημών Αγωγής', 'Παιδαγωγικό Τμήμα Δημοτικής Εκπαίδευσης', 'NULL'),
(927385006, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Σχολή Θετικών και Τεχνολογικών Επιστημών', 'Μουσείο Φυσικής Ιστορίας', 'NULL'),
(740365448, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Σχολή Θετικών και Τεχνολογικών Επιστημών', 'Τμήμα Επιστήμης και Τεχνολογίας Υλικών', 'NULL'),
(273703826, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Σχολή Θετικών και Τεχνολογικών Επιστημών', 'Τμήμα Επιστήμης Υπολογιστών', 'NULL'),
(926758651, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Σχολή Θετικών και Τεχνολογικών Επιστημών', 'Τμήμα Μαθηματικών', 'NULL'),
(5730825, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Τεχνική Υπηρεσία', 'NULL', 'NULL'),
(764838084, '2017-04-25 14:15:40', 'Πανεπιστήμιο Κρήτης', 'Φιλοσοφική Σχολή', 'Τμήμα Φιλοσοφικών και Κοινωνικών Σπουδών', 'NULL'),
(157406544, '2017-04-25 14:15:41', 'Πατριαρχική Ανωτάτη Εκκλησιαστική Ακαδημία Κρήτης', 'NULL', 'NULL', 'NULL'),
(715132565, '2017-04-25 14:15:41', 'Πειραματικό Γυμνάσιο', 'NULL', 'NULL', 'NULL'),
(550602615, '2017-04-25 14:15:41', 'Περιφέρεια Κρήτης', 'NULL', 'NULL', 'NULL');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `IaaS`
--

CREATE TABLE `IaaS` (
  `vmName` varchar(64) NOT NULL,
  `hypervisor` varchar(64) DEFAULT NULL,
  `convirtVmID` varchar(64) DEFAULT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TicketID` int(6) NOT NULL DEFAULT '0',
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modificationTicketID` int(6) NOT NULL DEFAULT '0',
  `personInChargeID` int(11) DEFAULT NULL,
  `CarrierID` int(11) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `state` enum('Running','Suspended','Removed','Quarantined','Stopped') NOT NULL DEFAULT 'Running',
  `status` enum('Production','Demo','Temporar') NOT NULL DEFAULT 'Production',
  `vmInformation` varchar(255) DEFAULT NULL,
  `sqlEntryCreatedBy` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `IaaS`
--

INSERT INTO `IaaS` (`vmName`, `hypervisor`, `convirtVmID`, `creationDate`, `TicketID`, `modificationDate`, `modificationTicketID`, `personInChargeID`, `CarrierID`, `expirationDate`, `state`, `status`, `vmInformation`, `sqlEntryCreatedBy`) VALUES
('A2-pm-60563', 'cb2-6.datacenter.uoc.gr', 'b5f72938-c8e0-3555-e2b6-304ee99335b1', '2014-10-02 05:40:11', 0, '2018-04-20 10:46:26', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('A2-pm-60563-dev', '', 'c68629ed-3cba-10c9-0a8d-89f3a4768d37', '2015-03-18 14:22:54', 0, '2016-12-29 14:57:16', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('aeahk', 'cb2-3.datacenter.uoc.gr', 'a23a60f8-ffc8-6640-1219-bb46d6be98be', '2012-11-24 23:59:59', 0, '2018-02-19 21:38:28', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('aheevaacs', 'cb3-2.datacenter.uoc.gr', '1a848d66-4e9d-724a-d82b-64341a2f6e1a', '2016-05-01 09:17:44', 0, '2018-04-20 10:46:31', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('alcyone', 'cb2-3.datacenter.uoc.gr', 'af2bc9f6-d54c-f7e3-ea45-0471151920a3', '2013-02-21 10:19:33', 0, '2016-12-29 14:57:15', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('andromeda', 'cb2-3.datacenter.uoc.gr', '0707d147-4598-45c5-bd8c-16094f463345', '2013-08-01 13:32:31', 0, '2016-12-29 14:57:01', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ASAv', '', '8a9ca311-6a62-cbee-70d0-fdbc608f9d1c', '2015-02-05 10:56:15', 0, '2016-12-29 14:57:12', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('aspam-avir', 'cb2-3.datacenter.uoc.gr', 'd8b0f2c1-dd68-8071-abd4-5a71467fb690', '2012-11-24 23:59:59', 0, '2018-04-20 10:46:35', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('bbb-ucnet', 'cb1-5.datacenter.uoc.gr', '59c8640e-f0a4-a98f-00fe-cd4b70f29112', '2018-01-25 06:48:28', 0, '2018-04-20 10:46:36', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('biotalent-65807', 'cb1-5.datacenter.uoc.gr', '0231ebf0-a2eb-1f80-438b-9f3e05df8fbb', '2016-12-22 07:24:12', 0, '2018-04-20 10:46:38', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('bs', 'cb1-6.datacenter.uoc.gr', 'fb7f244c-15dc-0af0-584d-3f301e488e82', '2013-03-11 07:20:08', 0, '2018-04-20 10:46:40', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('cardisoft-68014', 'cb1-5.datacenter.uoc.gr', 'bb4df3d6-017f-2b93-2334-31e2f5587fed', '2017-12-14 07:33:41', 0, '2018-04-20 10:46:41', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('cassiope', 'cb2-4.datacenter.uoc.gr', '60ea9de2-d74a-0939-2511-ca353507b075', '2013-11-30 13:45:46', 0, '2016-12-29 14:57:08', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ckan', '', '061a709b-cc39-1b76-1bb5-51b3c201f303', '2013-06-03 04:59:49', 0, '2017-12-03 20:26:46', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ckan-CRETE', 'cb2-3.datacenter.uoc.gr', 'd484cb7b-5ed2-5297-a30d-1205d766c4bd', '2014-01-09 11:03:06', 0, '2017-12-03 20:27:17', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ckan-CY', '', '94634e4e-b0b7-ad32-0e31-b06585254dbb', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ckan-HER', 'cb2-3.datacenter.uoc.gr', 'd90b9b11-754d-b68d-ab49-b2b4dc2e5731', '2013-11-19 12:47:33', 0, '2016-12-29 14:57:18', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('cleano', 'cb2-3.datacenter.uoc.gr', '06c12c06-321d-a028-6852-e31221de64dd', '2013-02-21 08:05:50', 0, '2017-06-28 15:26:11', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('cleano2', 'cb2-1.datacenter.uoc.gr', 'e532de89-e713-a7d8-afa2-379866fcf504', '2015-06-25 09:32:42', 0, '2016-12-29 14:57:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('cleano3', 'cb1-6.datacenter.uoc.gr', '8f0f8fc1-1443-c73f-8ddb-4306344fc7c4', '2016-01-28 13:34:30', 0, '2018-04-20 10:46:52', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-62224', 'cb1-7.datacenter.uoc.gr', '7360deab-9934-355c-45b4-010789eb80e6', '2015-09-15 06:12:24', 0, '2018-04-20 10:46:54', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-62422', '', '8bc5641e-38be-4f21-85d3-2a629cfd96e5', '2015-10-02 13:32:08', 0, '2018-02-19 21:38:26', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-65191', 'cb1-5.datacenter.uoc.gr', '3553ab07-d3f6-ce1a-8a52-aefa15c53ef0', '2016-10-14 12:34:01', 0, '2018-04-20 10:46:58', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-66376', 'cb1-5.datacenter.uoc.gr', '169cc836-7dd3-da4c-4231-cf61b4389ed5', '2017-03-17 06:28:26', 0, '2018-04-20 10:47:00', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-mta-64107', 'cb1-7.datacenter.uoc.gr', '4ffc328f-3bc6-820b-6a37-346562186aa9', '2016-03-29 09:26:20', 0, '2018-04-20 10:47:02', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('csd-www', 'cb1-7.datacenter.uoc.gr', 'd7e1abb3-a44f-866e-d238-4966a316bf0c', '2017-02-01 06:16:00', 0, '2018-04-20 10:47:04', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('db-dimoi', 'cb1-5.datacenter.uoc.gr', '614a5d95-0de4-7a6e-b25e-e5e3f0ebff2b', '2012-11-24 23:59:59', 0, '2018-04-20 10:47:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbc1-61504-1', 'cb1-1.datacenter.uoc.gr', '5332f161-5594-76f9-e4bb-2684b7786751', '2015-02-25 14:07:24', 0, '2018-04-20 10:47:07', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbc1-61504-2', 'cb2-3.datacenter.uoc.gr', 'ebdd965d-6fa6-af90-97e0-cd81784d1add', '2015-02-25 14:08:46', 0, '2018-04-20 10:47:07', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbc1-61504-3', 'cb1-1.datacenter.uoc.gr', 'a77b7e3a-4129-40c3-7c9b-96d160f8bfc3', '2015-03-18 08:36:44', 0, '2018-04-20 10:47:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbc1-61504-4', 'cb2-3.datacenter.uoc.gr', '2afc3cbc-6adf-70c5-83c4-e667cbd99581', '2015-03-18 08:45:22', 0, '2018-04-20 10:47:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbc1-61504-5', 'cb1-1.datacenter.uoc.gr', 'd3e829d7-28fc-1e13-7447-a67189308b43', '2015-03-18 08:49:38', 0, '2018-04-20 10:47:10', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dbconnect', 'cb2-6.datacenter.uoc.gr', '34ff7a12-d4e4-3761-0fa4-b55ed13eab42', '2012-11-24 23:59:59', 0, '2018-02-19 21:38:17', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('delos-59861', 'cb1-5.datacenter.uoc.gr', 'a342dfe9-c376-a0a2-2a02-4a2c75ea4988', '2014-07-10 16:28:50', 0, '2018-04-20 10:47:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('demo-windows-vlan800', '', 'c89631d0-8f39-fa6a-55f1-de60823a3a79', '2012-11-24 23:59:59', 0, '2017-12-03 20:27:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dev-ked', 'cb2-3.datacenter.uoc.gr', 'f0e5e3c4-8565-fb5a-39dc-69a5e5adc236', '2013-08-01 06:15:21', 0, '2016-12-29 14:57:21', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('dev-ked-win7', '', '33310571-3504-6187-5c89-b5819735b696', '2015-09-16 13:54:01', 0, '2016-12-29 14:57:05', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('dev4.tsl.gr_60053', 'cb2-4.datacenter.uoc.gr', 'bd2cc310-2593-c240-72c7-93b92a715ae2', '2014-07-17 05:53:45', 0, '2018-04-20 10:47:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ediamme-vm', 'cb2-6.datacenter.uoc.gr', '1e0d0fd3-641a-f564-80ac-218b9ce43a4e', '2012-11-24 23:59:59', 0, '2018-02-19 21:38:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('elearn', 'cb1-6.datacenter.uoc.gr', '39d168b1-eb85-6033-3018-68e6dc460699', '2013-07-03 06:30:34', 0, '2016-12-29 14:57:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('electra', 'cb2-4.datacenter.uoc.gr', 'fab76e3d-8984-811e-da49-97970f3bd8f5', '2013-10-24 09:51:28', 0, '2018-04-20 10:47:23', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ELKE_58805', 'cb1-5.datacenter.uoc.gr', '047b2d31-cfc0-8507-3682-c262881a327a', '2014-06-18 07:23:39', 0, '2018-04-20 10:47:24', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ELKE_65026', 'cb2-3.datacenter.uoc.gr', '1ce708b2-643f-e970-713c-46470a0fba23', '2016-10-03 09:45:44', 0, '2018-04-20 10:47:25', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ELKE_DB', 'cb2-4.datacenter.uoc.gr', '3db9aa69-87e5-9ebb-44a3-890ba794a75b', '2013-08-05 06:10:58', 0, '2016-12-29 14:57:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ELLAK_SRV', '', '141187c7-1f53-8680-6094-6e11c19cd1cf', '2014-05-08 06:05:30', 0, '2017-12-03 20:27:13', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_SRV2', '', '3f3d7274-31fc-7edd-a2ac-7a0e6eaa84f7', '2015-06-03 08:50:58', 0, '2016-12-29 14:57:07', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_SRV3', '', '821191be-95d9-6d13-7e26-58c3bfc314be', '2015-07-20 09:03:40', 0, '2016-12-29 14:57:11', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_vTHIN1', '', '0a61591b-8516-2ba2-235d-b19f90cc834f', '2014-05-21 08:18:29', 0, '2016-12-29 14:57:01', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_vTHIN2', '', 'b288b321-bfa6-7f2d-0898-7e4f4e3c59ba', '2014-05-21 10:28:03', 0, '2016-12-29 14:57:15', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_vTHIN3', '', 'a74af2c7-f64b-a180-09a3-18a4846cd8c8', '2014-05-21 10:28:47', 0, '2016-12-29 14:57:14', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_vTHIN4', '', 'e317b4d5-b6ba-60cb-7059-ef60513e5dfd', '2014-06-06 10:55:35', 0, '2016-12-29 14:57:19', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ELLAK_vTHIN5', '', 'e7374733-03da-875c-e955-f96414f2dfd2', '2014-06-12 12:09:03', 0, '2016-12-29 14:57:20', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('EMCESRS', 'cb2-6.datacenter.uoc.gr', '806b6361-02ce-8a33-632e-f66c9a0e86ba', '2014-06-20 13:04:46', 0, '2017-12-03 20:27:15', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('eschool-med', 'cb1-5.datacenter.uoc.gr', '71c362cf-42ae-1a88-a32b-6bdcdc9605c2', '2015-07-24 06:50:08', 0, '2016-12-29 14:57:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('fksserver', 'cb2-4.datacenter.uoc.gr', '2de06a61-4c6b-d69a-4982-2e9075ba682d', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:05', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('gunet-idp', '', '6ffd8115-4230-8536-26a4-1d9a84228824', '2015-04-02 12:18:33', 0, '2017-06-28 12:11:00', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-ldap-translucent', '', '4862423b-508a-6ce0-29c8-dc1871c98eec', '2015-04-02 13:27:36', 0, '2017-06-28 12:10:59', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-mypassword', '', 'f124de35-2d51-57f0-e6d3-713174206bd1', '2015-04-02 13:24:03', 0, '2017-06-28 12:11:03', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-sso-new', '', '8bcd4859-86d5-70be-4444-889666468d08', '2015-04-02 09:57:42', 0, '2017-06-28 12:11:00', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-uod', '', '973eac7f-1dcd-abb6-726a-be4b419a14af', '2015-04-03 06:53:54', 0, '2017-06-28 12:11:01', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-uregister', '', '7d06e139-2409-962d-b993-d0d5c3b41524', '2015-04-02 13:14:15', 0, '2017-06-28 12:11:00', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('gunet-vd', '', '02e01afc-aae8-5d3f-7077-40b13102699c', '2015-04-03 06:50:55', 0, '2017-06-28 12:10:57', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('hack4med', '', '9e60a097-3b08-9e6e-8f5b-d17fc049895e', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:14', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('hpcstats-demo', '', 'fc060d3d-34de-bdf9-3413-844df7df8550', '2012-11-24 23:59:59', 0, '2017-12-03 20:27:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ILYDA-66954', 'cb3-2.datacenter.uoc.gr', '42417892-86e5-73b2-2b70-677b3a11e7fb', '2017-07-17 07:37:20', 0, '2018-04-20 10:48:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ILYDA-App1-61867', '', '2da1ccda-8f10-39bb-2ea9-66b884da5825', '2015-05-29 06:59:48', 0, '2018-02-19 21:38:39', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ILYDA-App2-62883', '', '3cde2fcb-8f37-d740-35bd-59a1906548d5', '2015-11-04 07:16:05', 0, '2017-12-03 20:26:54', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ILYDA-Db1-61867', '', 'c2a72690-1ba2-c643-53de-19c2f07942c2', '2015-06-03 07:50:58', 0, '2016-12-29 14:57:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ILYDA-Db2-66477', 'cb2-1.datacenter.uoc.gr', '59989c0a-6da7-4e92-57f7-02917663fea1', '2017-03-31 05:50:15', 0, '2018-04-20 10:48:26', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ILYDA-Web1-61867', '', '42269c89-ee1f-03b6-ed53-6575e95b8dde', '2015-05-29 07:02:37', 0, '2018-02-19 21:38:40', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('jpat-Win8', 'cb3-2.datacenter.uoc.gr', '7784e287-c6de-8eb8-968e-e726cea56a00', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:31', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('kalogerix', '', '0b083658-defa-59eb-c34a-c622f4558575', '2013-04-18 05:34:51', 0, '2016-12-29 14:57:02', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('knossos', 'cb2-4.datacenter.uoc.gr', '16f3db17-5a19-0643-fe2b-381e6634a975', '2013-08-20 08:03:29', 0, '2016-12-29 14:57:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('knossos2', 'cb2-6.datacenter.uoc.gr', 'dd95eb1c-6d35-d26b-33e3-a72ace4087b5', '2013-08-04 15:34:11', 0, '2016-12-29 14:57:19', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('ldapdc', 'cb2-1.datacenter.uoc.gr', '6104a21f-2217-7b12-929e-110a9e716c82', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:38', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('lib-67680-ejournals', 'cb3-2.datacenter.uoc.gr', '4b2367d0-c7dd-377e-14a7-61d678c8ece4', '2017-11-03 09:20:57', 0, '2018-04-20 10:48:40', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('libserver1', 'cb2-4.datacenter.uoc.gr', '6807529e-1175-9750-40fb-59522484f09a', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:41', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('libserver2', 'cb1-7.datacenter.uoc.gr', '15429dee-f2b7-b517-5d63-14e9e9afeda2', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:43', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('libserver3-GIT', 'cb2-4.datacenter.uoc.gr', 'd3b5f22b-f8db-17b5-0cbf-2b2b253e0446', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:44', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('libserver5', 'cb2-4.datacenter.uoc.gr', '73e8af12-99f7-5b9d-b1dd-48224dcb539f', '2013-05-16 10:20:27', 0, '2018-04-20 10:48:44', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('lib_anemi', 'cb2-1.datacenter.uoc.gr', '2cf8f43d-dabc-85a8-28ce-31801fee3e54', '2015-03-30 07:08:58', 0, '2018-04-20 10:48:47', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('lib_e-locus', 'cb2-5.datacenter.uoc.gr', 'e3844d28-413f-85f5-c1c4-95c4b9765c0c', '2014-05-06 11:22:10', 0, '2018-04-20 10:48:47', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('lib_soranos', 'cb2-5.datacenter.uoc.gr', 'e0c5e07b-01ef-fa9a-3191-37e3488253b9', '2014-06-02 18:27:57', 0, '2018-04-20 10:48:48', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('loadbalancer', 'cb2-3.datacenter.uoc.gr', '4153df79-f72f-f1b1-f583-b7939f4d4ef9', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:49', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('logserver', 'cb2-4.datacenter.uoc.gr', '09b8f439-d497-1afa-6e84-788340a0c9c1', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:01', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('LTSP-math-66702', '', 'f14ae011-102a-c2e4-586d-820dd547e0ad', '2017-06-27 08:24:15', 0, '2017-12-03 20:27:12', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('LTSP-math-66702-thin1', 'cb3-2.datacenter.uoc.gr', '326b4d54-a0cb-2176-aafc-1930265ce7fd', '2017-07-14 08:47:22', 0, '2018-04-20 10:48:54', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('LTSPSrv', '', '86edb320-6f56-5df4-b9fe-10d9975ec855', '2013-05-28 05:53:04', 0, '2017-12-03 20:27:00', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('mail-management', 'cb2-3.datacenter.uoc.gr', 'ed2c8080-0da4-768b-42bf-5c0976b130bd', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:57', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('maillists', 'cb2-4.datacenter.uoc.gr', '9b29dad5-209a-89a6-5263-cddc6c41dc27', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('math-server', 'cb2-3.datacenter.uoc.gr', 'ed2dc333-f72c-5be1-1586-3d49335293dd', '2012-11-24 23:59:59', 0, '2018-04-20 10:48:58', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('math-server2', 'cb1-7.datacenter.uoc.gr', 'a9a4cde4-fbbc-83fc-b494-777fbf4acb60', '2013-10-21 08:37:54', 0, '2016-12-29 14:57:14', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('math-server3', 'cb2-3.datacenter.uoc.gr', 'e1cfa06b-aa68-0430-5906-3f782c6ffa1d', '2013-11-01 10:29:51', 0, '2016-12-29 14:57:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('math-server4-61439', 'cb1-7.datacenter.uoc.gr', 'bbf1a1df-2b77-4f87-90aa-75935ac3f916', '2015-03-23 12:19:14', 0, '2018-04-20 10:49:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('med-59403', 'cb2-3.datacenter.uoc.gr', '8465ea27-9a9d-65fa-a643-d6e549abff15', '2014-06-20 05:40:49', 0, '2016-12-29 14:57:11', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('med-60162', 'cb2-3.datacenter.uoc.gr', 'afe99fe9-00ec-5979-3357-82aaafd5443e', '2014-07-30 05:42:14', 0, '2017-04-25 12:10:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('med-hydra', 'cb3-2.datacenter.uoc.gr', '4b9dde43-4cf7-470f-c20d-b581b67f9db1', '2017-10-20 10:43:23', 0, '2018-04-20 10:49:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('mediawiki', 'cb2-3.datacenter.uoc.gr', '38776c6a-6ef4-679a-8b0c-0234d8a8b638', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('med_57105', 'cb2-3.datacenter.uoc.gr', '0adb089e-c70e-8fde-b2bb-80e8ee368263', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('med_57885', 'cb2-5.datacenter.uoc.gr', '8268fd1f-476a-fd9f-c52e-cf585c496963', '2013-11-15 09:17:14', 0, '2016-12-29 14:57:11', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('merope', '', '8ff72bac-ac98-49ee-9e57-7d4d74b0964a', '2014-07-28 19:14:08', 0, '2016-12-29 14:57:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('mmlab_58600', 'cb2-4.datacenter.uoc.gr', 'a1aff578-caa6-9445-40fd-9493c0c49d36', '2013-12-23 07:18:31', 0, '2018-04-20 10:49:10', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('mon', 'cb2-3.datacenter.uoc.gr', '6e12a90a-6cb5-196d-8052-83f766fb52a5', '2013-05-21 14:11:39', 0, '2018-04-20 10:49:10', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('mta', 'cb2-6.datacenter.uoc.gr', 'cab6bb0b-c3d1-b8a8-1105-65869262a7c9', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:11', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nfssrv', 'cb2-6.datacenter.uoc.gr', '231fa8fd-30b3-5a0b-bd57-6bb423473077', '2014-01-22 20:05:55', 0, '2018-04-20 10:49:12', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nhmc', 'cb1-5.datacenter.uoc.gr', '5e620bf4-9488-5f53-2696-19338083f0e1', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:14', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nhmc-mobile_58851', 'cb2-4.datacenter.uoc.gr', '31eae318-1fa8-03ce-1a55-e9b218600562', '2014-02-12 09:30:29', 0, '2018-04-20 10:49:15', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nhmc_webGIS', 'cb1-5.datacenter.uoc.gr', 'e45d1744-d890-7397-544c-6bb621d1f111', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('Nikolaou-vm', 'cb1-5.datacenter.uoc.gr', '4a7e2702-0bd7-a6df-44a3-5e225984cf1c', '2012-11-24 23:59:59', 0, '2018-04-20 10:49:18', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nms_60930_A', 'cb2-5.datacenter.uoc.gr', 'e8418384-ac11-2664-ca38-4f920cd055f6', '2014-11-13 06:59:08', 0, '2018-04-20 10:49:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nms_60930_B', 'cb2-5.datacenter.uoc.gr', '1c07ed72-e2b6-846e-d39d-52e8f4d3547e', '2014-11-20 07:54:00', 0, '2018-04-20 10:49:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('nms_60930_E', 'cb2-5.datacenter.uoc.gr', '541c3ca5-ee37-abab-e0d1-57ea390e05fa', '2014-11-25 10:58:43', 0, '2018-04-20 10:49:20', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('old-cleano', 'cb2-1.datacenter.uoc.gr', '2436fef5-4136-2c22-f8cc-e29caa12cc07', '2015-06-02 18:37:55', 0, '2018-04-20 10:49:23', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('oldeschool-med', 'cb1-5.datacenter.uoc.gr', '732714c1-7994-892e-2ce9-a3861772d617', '2015-07-21 08:22:28', 0, '2018-04-20 10:49:24', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('om_ucnet_60551', '', 'e881415d-bb95-fff3-e079-79878655f6a2', '2014-10-01 06:23:42', 0, '2016-12-29 14:57:20', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('openeclass', 'cb2-3.datacenter.uoc.gr', 'd1fa1930-47e7-c506-8112-911e50768aa5', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:17', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('OpenSIPS_2.1_VM', '', '99b49dae-9935-2f93-69ae-83abda2323b6', '2015-07-27 09:58:52', 0, '2018-02-19 21:38:41', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('OTS_61366_app1', 'cb2-1.datacenter.uoc.gr', '2277962b-04dd-52de-d23b-be57826e0d1a', '2015-02-09 06:41:41', 0, '2018-04-20 10:49:32', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('OTS_61366_app2', 'cb2-1.datacenter.uoc.gr', '8c481863-d7a7-beac-ee7a-1ff445bf85af', '2015-02-09 09:14:38', 0, '2018-04-20 10:49:35', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('OTS_61366_db1', 'cb2-1.datacenter.uoc.gr', '85f04507-5533-5270-7454-1c5b63136b60', '2015-02-09 09:46:14', 0, '2018-04-20 10:49:37', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('OTS_62427_web2', 'cb2-1.datacenter.uoc.gr', 'bc4d12d2-2e18-e268-d00d-0c05cad0e37e', '2015-09-18 10:05:58', 0, '2018-04-20 10:49:40', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('owncloud', '', 'd844b931-5ff4-95e2-f9b6-5b109a39ba4a', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:18', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('peir-gym-ira', 'cb2-3.datacenter.uoc.gr', '058a7a22-352d-512c-dd69-5107310b493e', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:00', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('PeriferiaDCtun-Srv', 'cb2-1.datacenter.uoc.gr', 'c5c72258-4251-398b-9d6a-6ddb97613380', '2017-09-18 07:20:40', 0, '2018-04-20 10:49:45', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_57530_app', 'cb1-1.datacenter.uoc.gr', '886836af-799a-ac26-2afb-14d20abf34cb', '2013-05-17 05:38:51', 0, '2018-04-20 10:49:47', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_57530_db', 'cb3-2.datacenter.uoc.gr', 'e7902b72-13f2-0308-637e-19cb0565ba3a', '2013-05-17 06:12:08', 0, '2018-04-20 10:49:49', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_59070_ubuntu', 'cb2-3.datacenter.uoc.gr', 'ad0ab180-de45-aeb9-f92e-89b31482c2a1', '2014-02-27 09:48:15', 0, '2018-04-20 10:49:50', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_64574_app', 'cb3-2.datacenter.uoc.gr', 'a730b27b-1c88-c2ed-65b4-ace8059c78ff', '2016-06-22 10:26:37', 0, '2018-04-20 10:49:52', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_64574_db', 'cb3-2.datacenter.uoc.gr', 'b6577177-52d9-01f4-5a56-14a5e7d58fca', '2016-06-22 10:29:29', 0, '2018-04-20 10:49:55', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_65714_vhosting', 'cb1-5.datacenter.uoc.gr', 'c80ac5b6-9d00-dfc2-74b9-35c79427e276', '2016-12-06 11:08:53', 0, '2018-04-20 10:49:56', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_admin1', 'cb3-2.datacenter.uoc.gr', '1c0e6e50-aca0-e161-0c51-f12f5e17b963', '2017-07-04 05:51:41', 0, '2018-04-20 10:49:59', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_hadb', 'cb1-6.datacenter.uoc.gr', '0136227c-51a1-8f45-238e-d10b2440956e', '2017-06-21 08:21:36', 0, '2018-04-20 10:50:01', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_hapublic', 'cb1-1.datacenter.uoc.gr', '1254846e-9c05-c1e0-f63e-c93b3982ca02', '2017-07-03 06:06:38', 0, '2018-04-20 10:50:02', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_mdb01', 'cb3-2.datacenter.uoc.gr', '01a2e81d-4761-9071-45a7-1c6770501838', '2017-06-14 07:40:34', 0, '2018-04-20 10:50:04', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_mdb02', 'cb1-5.datacenter.uoc.gr', '828cad0e-86ba-da63-9f43-80deaaf8c525', '2017-06-14 07:49:53', 0, '2018-04-20 10:50:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_mdb03', 'cb2-5.datacenter.uoc.gr', '32f2f873-f93d-224b-8d9d-bf0c4a979a01', '2017-06-14 07:55:10', 0, '2018-04-20 10:50:07', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_web1', 'cb1-6.datacenter.uoc.gr', 'd8c8c817-9281-82e4-3a0b-5a5e8c26d555', '2017-07-03 06:31:46', 0, '2018-04-20 10:50:08', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_ekloges_web2', 'cb2-6.datacenter.uoc.gr', 'be78c0c4-06b7-e330-458f-53d50938f991', '2017-07-04 05:15:03', 0, '2018-04-20 10:50:10', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('periferia_papyros', '', '199a45d3-e190-4df5-1f58-a26a83548a28', '2015-10-09 06:55:20', 0, '2016-12-29 14:57:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('Phl1_Reth', 'cb1-7.datacenter.uoc.gr', 'f7f6b95b-aaba-763b-724d-0a50ddc38697', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:14', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('plex_57141', 'cb1-7.datacenter.uoc.gr', '01890eca-fed5-fdfb-42ce-dfff668ba6db', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('pop-imap', 'cb2-3.datacenter.uoc.gr', '5c09acdb-6620-7e4a-dde3-ac4c704ecfdb', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('PTDE_66588', 'cb3-2.datacenter.uoc.gr', '587d33ea-19aa-4f8c-a715-0cbac6850a2a', '2017-04-28 10:33:40', 0, '2018-04-20 10:50:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('radius1', 'cb2-3.datacenter.uoc.gr', 'd73f4871-3019-8ca0-ffa2-d5ac4713389b', '2014-03-20 07:06:03', 0, '2018-02-19 21:38:33', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('radius2', 'cb1-5.datacenter.uoc.gr', '4ead709c-4557-8b30-f667-fb1b2244952e', '2016-09-29 11:46:02', 0, '2018-04-20 10:50:21', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('seafile-srv', '', '15e9a616-706f-6330-fe22-528382c3814b', '2015-09-17 12:47:13', 0, '2017-12-03 20:26:50', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-db-1', 'cb2-1.datacenter.uoc.gr', '7c6a50a4-cf79-9ecf-1018-5790a2628199', '2015-05-13 13:54:31', 0, '2018-04-20 10:50:26', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-rports-1', 'cb1-6.datacenter.uoc.gr', 'c35804db-0c76-bf0d-9842-d68ecb33c1d2', '2015-05-14 08:32:36', 0, '2018-04-20 10:50:28', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-1', 'cb2-1.datacenter.uoc.gr', '17752618-c001-b303-b389-c9b6bc08b13c', '2015-05-13 13:44:58', 0, '2018-04-20 10:50:30', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-2', 'cb2-1.datacenter.uoc.gr', 'ec6a39e0-df84-bc75-6f11-2a71286df2ad', '2015-09-23 07:10:43', 0, '2018-04-20 10:50:32', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-3', 'cb1-6.datacenter.uoc.gr', 'dcc13363-e40e-511b-1ea6-92ca749c8c02', '2015-09-23 07:18:35', 0, '2018-04-20 10:50:34', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-4', 'cb1-6.datacenter.uoc.gr', 'c1bc2334-782a-83f2-e7b7-c8b36d341f0f', '2015-09-23 07:20:28', 0, '2018-04-20 10:50:36', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-5', 'cb1-7.datacenter.uoc.gr', '879ca113-0640-3862-4a75-ba17305f5777', '2015-09-23 08:07:03', 0, '2018-04-20 10:50:38', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-6', 'cb1-7.datacenter.uoc.gr', 'd1470073-df83-5fdf-d430-e7bb2495e919', '2015-09-23 08:08:57', 0, '2018-04-20 10:50:40', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-7', 'cb1-7.datacenter.uoc.gr', '138875f7-49d7-a9be-28fc-1c5047d98283', '2015-09-23 08:16:20', 0, '2018-04-20 10:50:42', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secr-web-lb', '', '996734b7-cf1f-7b88-f514-01c6a33a6b5c', '2015-09-23 08:52:11', 0, '2018-02-19 21:38:41', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('secrapp-class-1', 'cb1-6.datacenter.uoc.gr', '8e445ea1-f627-b91c-deb3-9a2c131d33cf', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:47', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secrapp-stdnt-1', 'cb1-7.datacenter.uoc.gr', '6aeaf271-cc3e-8cb6-e455-34f099402323', '2015-05-13 13:04:53', 0, '2018-04-20 10:50:49', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('secrweb-profs-1', 'cb1-7.datacenter.uoc.gr', 'cae9e3e7-9536-b739-63d1-244f655d14c6', '2015-05-14 08:30:10', 0, '2018-04-20 10:50:51', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('shibboleth', 'cb2-3.datacenter.uoc.gr', '6f265dd5-f7fb-4437-4bd0-1a42574850f3', '2013-07-11 13:02:09', 0, '2016-12-29 14:57:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('sites-vits', 'cb2-5.datacenter.uoc.gr', 'bd04326c-9f0f-99ae-2819-6d2d24a20387', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:52', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('soc-66915', 'cb3-2.datacenter.uoc.gr', 'dbcd8baf-1642-6d27-a0e6-1db5831ad193', '2017-06-29 08:41:48', 0, '2018-04-20 10:50:54', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('sso', 'cb2-1.datacenter.uoc.gr', '20d6e5f8-fd3d-c0ff-fb0d-48008df4a2e4', '2012-11-24 23:59:59', 0, '2018-04-20 10:50:57', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('stats-ucnet', '', '8c587571-16ec-85d9-10e7-e219d86469e1', '2014-07-28 06:04:25', 0, '2016-12-29 14:57:12', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('stirizo-devel2', 'cb1-6.datacenter.uoc.gr', 'e01398c0-9d7f-38f9-f5ee-341f6942bde4', '2012-11-24 23:59:59', 0, '2018-04-20 10:51:01', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('stirizo-devel2-PSD', 'cb1-6.datacenter.uoc.gr', '3ddb46ad-09dc-fff0-0034-ff2641c7baf7', '2012-11-24 23:59:59', 0, '2018-04-20 10:51:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('stirizo-devel3', '', '7a9c2775-51e4-5944-4865-ea0e240cc79d', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:10', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('stirizo-devel3-PSD', '', 'd69ff9e7-1ef2-dbea-25eb-64d21d75ab1c', '2013-08-05 13:44:51', 0, '2016-12-29 14:57:18', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('synnefo-node1', '', '366b29cf-bc00-2b46-f83d-75c9245c37d5', '2016-10-17 11:00:59', 0, '2018-02-19 21:38:39', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('synnefo-node2', '', '02c179f5-d5b6-15d7-cbe7-4883cab69fe8', '2016-10-17 11:02:29', 0, '2018-02-19 21:38:38', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('tacacs', 'cb1-6.datacenter.uoc.gr', '3cec414e-9667-dca1-520e-3b4b27a9d315', '2015-11-20 10:49:11', 0, '2018-04-20 10:51:15', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('telesforos', 'cb1-6.datacenter.uoc.gr', 'ac1df426-7d77-3703-6237-524c31a16fbc', '2012-11-24 23:59:59', 0, '2018-04-20 10:51:17', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('test-VM-jpat-32G', '', '06da77f0-8631-d7a6-dec4-9413dee2150e', '2015-03-11 14:25:29', 0, '2016-12-29 14:57:01', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('test_vlan15', '', '703e7ce9-3993-9a84-0309-b1efa638ef1f', '2013-08-01 12:31:06', 0, '2016-12-29 14:57:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tety-64515', 'cb1-7.datacenter.uoc.gr', 'c4b93879-e3c9-b129-1ff7-017a2d8e130e', '2016-06-08 08:46:58', 0, '2016-12-29 14:57:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tety-s2', 'cb2-6.datacenter.uoc.gr', '7bae28a9-afab-6b8e-44e9-df95fc4ac757', '2013-11-01 11:37:41', 0, '2018-04-20 10:51:25', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tety-s3', 'cb2-3.datacenter.uoc.gr', '7ab59d58-1e46-b5c7-aeb5-7563cebb7c63', '2014-06-16 09:24:00', 0, '2018-04-20 10:51:25', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tetyserver1', 'cb2-1.datacenter.uoc.gr', '763bfcac-b3ae-3d1c-8174-4e555fd082b6', '2012-11-24 23:59:59', 0, '2018-04-20 10:51:28', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tety_s4_64733', 'cb2-6.datacenter.uoc.gr', '3ec44272-e21c-83f3-01f7-b7346243c2fd', '2016-07-29 06:16:36', 0, '2018-04-20 10:51:29', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_60624', 'cb2-3.datacenter.uoc.gr', '999860ac-0112-259e-292a-c17e5a316fd8', '2014-12-22 13:10:26', 0, '2018-04-20 10:51:29', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_64645', '', '4686a4bb-79a7-fa8a-ed8e-ad409be9b5d3', '2016-07-25 05:42:42', 0, '2017-12-03 20:26:55', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_64770_1', '', '038fba5e-22f8-84a5-4ed6-5229bafeea57', '2016-08-09 07:53:07', 0, '2016-12-29 14:57:00', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_2', '', 'c7490947-8a5d-1bc7-ac88-ad5067127897', '2016-08-09 08:06:50', 0, '2016-12-29 14:57:16', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_64770_2_m', '', '3f160a2a-97cd-5d01-ce12-e0ea7a55d8af', '2016-08-09 09:41:46', 0, '2016-12-29 14:57:07', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_64770_3', '', '07043088-cede-b623-c845-696c19cead60', '2016-08-09 08:25:46', 0, '2016-12-29 14:57:01', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('TicketID_64770_3_m', '', 'e828b81b-30f3-4719-5229-5fd61a44b16a', '2016-08-09 09:39:02', 0, '2016-12-29 14:57:20', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_4', '', 'f94f066a-55d4-06a8-16bc-45756f8841a3', '2016-08-10 20:35:15', 0, '2016-12-29 14:57:21', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_4_m', '', 'ab4d12a4-6c94-83fc-4970-d34d126a2394', '2016-08-10 20:38:29', 0, '2016-12-29 14:57:15', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_5', '', '2a899629-0d52-54b7-80f4-1cdffa911050', '2016-08-10 20:44:59', 0, '2016-12-29 14:57:04', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_5_m', '', '2bf5ff36-076e-fa61-79b7-fe931a8663b6', '2016-08-10 20:46:41', 0, '2016-12-29 14:57:05', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_6', '', '15efedae-d084-cc40-90a6-76e9dc1971f5', '2016-08-10 21:02:25', 0, '2016-12-29 14:57:03', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_64770_6_m', '', '4894100c-25cf-751d-1b03-0f0e2d9486dc', '2016-08-10 21:08:03', 0, '2016-12-29 14:57:07', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('TicketID_65409_lib', 'cb1-5.datacenter.uoc.gr', '92efac16-ce55-928f-50bd-4af13484c918', '2016-11-16 08:12:44', 0, '2018-04-20 10:52:02', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tmp-kaltura', '', '169614a9-551a-e60f-9cc3-a74a537cb9f3', '2013-10-01 08:45:34', 0, '2016-12-29 14:57:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tmp_59749', 'cb2-6.datacenter.uoc.gr', 'e8673f60-0516-bddb-5732-3e723ebc30ab', '2014-06-03 05:16:07', 0, '2018-04-20 10:52:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tms-vc-int', 'cb2-3.datacenter.uoc.gr', 'a175a15c-6b6c-748a-10b2-4b39c28a9ff8', '2013-11-19 10:56:16', 0, '2016-12-29 14:57:14', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tolis_59192', '', '43995b31-3b25-2829-8e3a-c66f2b60649c', '2014-03-17 07:03:04', 0, '2018-02-19 21:38:19', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('torps', 'cb2-6.datacenter.uoc.gr', 'dccbb17f-7737-b607-a0e4-c19d771fa4b8', '2014-04-02 05:52:49', 0, '2018-02-19 21:38:34', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('tyserv', 'cb2-3.datacenter.uoc.gr', '8140ed30-4905-549b-65ce-562f88b7ba42', '2013-08-04 20:53:24', 0, '2016-12-29 14:57:11', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('UCnetDCtun', '', 'd1731f93-db93-c075-8338-4453446e7aaa', '2015-05-26 20:25:00', 0, '2016-12-29 14:57:17', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('ultivpn', 'cb1-5.datacenter.uoc.gr', 'aba0eaba-c7fa-7660-863c-8a325b04f8eb', '2018-01-16 09:05:52', 0, '2018-04-20 10:52:14', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('vc-int', 'cb2-3.datacenter.uoc.gr', '3823d5b1-c019-1a10-a696-89350d840745', '2013-11-19 06:46:13', 0, '2016-12-29 14:57:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VDi-1-prytaneia', '', 'ff0a6ee9-073f-2ed0-5c69-27730f69c9f5', '2014-01-20 09:43:24', 0, '2017-04-25 12:10:26', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('velos', 'cb2-4.datacenter.uoc.gr', '9167fdca-2ff2-ce23-c2c1-7cad2d86f48a', '2013-08-07 11:48:00', 0, '2016-12-29 14:57:13', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('vhyp1', '', '800068e3-224b-36f6-2545-a95155b0d28a', '2015-01-26 09:06:00', 0, '2016-12-29 14:57:11', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('vod-telesforos', '', '27b64ae0-0bd9-8c47-22f5-58127960689d', '2014-01-28 06:41:19', 0, '2016-12-29 14:57:04', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('vod-ucnet', 'cb2-5.datacenter.uoc.gr', '496261bb-98ad-8586-d834-f922db2b7594', '2014-07-24 06:52:19', 0, '2018-04-20 10:52:24', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('voip-proxy', 'cb2-3.datacenter.uoc.gr', '18d3f171-8f9f-b973-bdaf-83801544af50', '2013-08-02 09:01:08', 0, '2018-02-19 21:38:15', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipMgmtsrv', 'cb2-6.datacenter.uoc.gr', '0ca1fed7-99bf-8d93-4e06-7f7f3437b5c0', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:25', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer-NNI1', 'cb1-6.datacenter.uoc.gr', '2d94e627-cca5-f86a-a90e-207de4d2f027', '2015-02-13 09:31:19', 0, '2018-04-20 10:52:27', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer2', 'cb2-6.datacenter.uoc.gr', '02407278-e241-3b68-f9a2-773c54824f2d', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:28', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer3', 'cb2-6.datacenter.uoc.gr', 'c70be200-923c-475e-21f5-f824fb993135', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:29', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer4', 'cb2-6.datacenter.uoc.gr', '06838fe2-be24-f979-5bc0-9e4ac44bbd4c', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:30', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer5-SBC1', 'cb2-6.datacenter.uoc.gr', '3ab3b25e-0ade-1901-ed63-d79e762d483a', '2014-06-26 10:03:09', 0, '2018-04-20 10:52:31', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer6-X11', 'cb2-6.datacenter.uoc.gr', 'b17b8682-814f-30f3-64b4-b2527b427c9c', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:32', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer7', 'cb2-6.datacenter.uoc.gr', '574d0410-4a9f-1060-0acb-e1f51d9e48d2', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:34', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer8', '', '3efd424a-c3e4-fcf5-49ee-2351dff4223a', '2012-11-24 23:59:59', 0, '2016-12-29 14:57:07', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer8_112', 'cb2-6.datacenter.uoc.gr', '7e4c4bc1-a1c3-161f-74a1-fa10f79c98c3', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:37', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer_fserv', '', '6fd24055-5b39-acb1-e1ba-3d2107320085', '2016-08-31 11:43:49', 0, '2016-12-29 14:57:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer_Hylafax_Server', '', '952cb767-c69a-3c33-e40e-d5839acf4419', '2015-09-24 09:41:33', 0, '2016-12-29 14:57:13', 0, NULL, NULL, NULL, 'Stopped', 'Production', NULL, NULL),
('VoipServer_MariaDB_node1', 'cb1-6.datacenter.uoc.gr', '9d38070e-244d-36fb-b7d5-ca0af3829e2b', '2015-08-19 07:10:39', 0, '2018-04-20 10:52:44', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('VoipServer_MariaDB_node2', 'cb1-1.datacenter.uoc.gr', 'a1e8480b-8d81-dcd9-e963-14288b6e3c33', '2015-08-19 07:13:59', 0, '2018-04-20 10:52:45', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('vpnserver2', 'cb2-6.datacenter.uoc.gr', '0a8bdc21-db2d-b17b-a507-18d5063ad041', '2013-11-20 10:30:25', 0, '2018-04-20 10:52:46', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('wcs-manager', '', 'fa3803db-3984-b001-8599-f6652ef95a63', '2013-08-02 07:46:05', 0, '2017-12-03 20:27:18', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('web', 'cb2-3.datacenter.uoc.gr', '276ca01f-f877-f604-98a4-fc13843fce4d', '2012-11-24 23:59:59', 0, '2018-04-20 10:52:49', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('Win7_THIN_DEMO', 'cb2-3.datacenter.uoc.gr', '6f8b66cb-5a05-5708-296a-30c685bf3b4a', '2013-05-21 05:38:24', 0, '2018-04-20 10:52:50', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('wowza-ucnet', 'cb3-2.datacenter.uoc.gr', 'd620b33c-0a50-58ee-cb9d-ba3cf56e539b', '2014-08-04 06:56:46', 0, '2017-12-03 20:27:08', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('www_ucnet', 'cb2-5.datacenter.uoc.gr', 'd352bf50-c73d-0a5d-1dee-aeb3fab48a7f', '2014-06-23 06:55:10', 0, '2018-04-20 10:52:53', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xalkis-61656-fs', 'cb1-6.datacenter.uoc.gr', 'dd0b6e17-1d0f-e418-cadb-9ee94684ad9b', '2015-04-23 09:18:08', 0, '2018-04-20 10:52:55', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xalkis-62930-Win7', 'cb1-6.datacenter.uoc.gr', 'dd60db6a-5f30-00b3-b3a9-25431dec0c3e', '2015-10-26 10:07:06', 0, '2018-04-20 10:52:57', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xalkis-65677', 'cb2-1.datacenter.uoc.gr', 'c030aa04-2aa2-e457-eb15-69ef8687cdf0', '2016-12-14 10:06:12', 0, '2018-04-20 10:52:59', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xalkis-66797', 'cb3-2.datacenter.uoc.gr', 'b6fb8c96-1e3f-02d7-2377-20d127f2c47b', '2017-05-29 12:20:07', 0, '2018-04-20 10:53:03', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xalkis-66910', 'cb3-2.datacenter.uoc.gr', 'd7e9efde-fb5a-3088-c0bd-c267c5ec00d5', '2017-06-26 11:35:46', 0, '2018-04-20 10:53:06', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL),
('xenios', 'cb2-3.datacenter.uoc.gr', '66eb6384-137b-11ca-395e-756a707e316f', '2013-02-22 10:48:06', 0, '2016-12-29 14:57:09', 0, NULL, NULL, NULL, 'Running', 'Production', NULL, NULL);

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `Owners`
--

CREATE TABLE `Owners` (
  `ID` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ownerName` varchar(254) NOT NULL,
  `ownerSurname` varchar(254) NOT NULL,
  `ownerEmail` varchar(254) NOT NULL,
  `ownerPhoneNum` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `PersonInCharge`
--

CREATE TABLE `PersonInCharge` (
  `ID` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Name` varchar(254) NOT NULL,
  `Surname` varchar(254) NOT NULL,
  `Email` varchar(254) NOT NULL,
  `PhoneNum` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `Technicians`
--

CREATE TABLE `Technicians` (
  `ID` int(11) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `techName` varchar(254) NOT NULL,
  `techSurname` varchar(254) NOT NULL,
  `techEmail` varchar(254) NOT NULL,
  `techPhoneNum` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `vmCharacteristics`
--

CREATE TABLE `vmCharacteristics` (
  `vmName` varchar(254) NOT NULL,
  `firstTicketID` int(6) DEFAULT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modificationTicketID` int(6) NOT NULL DEFAULT '0',
  `vCores` enum('1','2','4','6','8','10','12','16','24') NOT NULL DEFAULT '2',
  `vRam` enum('0.5','1','2','4','6','8','10','12','14','16','18','20','22','24','28','32') NOT NULL DEFAULT '2',
  `vNICs` enum('0','1','2','3','4','5','6') DEFAULT '1',
  `vHDDs` enum('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15') DEFAULT '1',
  `totalvHDDSizeInGB` decimal(10,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `vmCharacteristics`
--

INSERT INTO `vmCharacteristics` (`vmName`, `firstTicketID`, `creationDate`, `modificationDate`, `modificationTicketID`, `vCores`, `vRam`, `vNICs`, `vHDDs`, `totalvHDDSizeInGB`) VALUES
('A2-pm-60563', NULL, '2014-10-02 05:40:11', '2017-07-12 11:55:52', 0, '8', '8', '1', '1', '100.82800'),
('A2-pm-60563-dev', NULL, '2015-03-18 14:22:54', '2016-12-19 14:34:21', 0, '2', '4', '1', '1', NULL),
('aeahk', NULL, '2012-11-24 23:59:59', '2017-07-12 11:42:49', 0, '2', '1', '1', '1', '97.65620'),
('aheevaacs', NULL, '2016-05-01 09:17:44', '2017-07-12 11:55:55', 0, '2', '4', '1', '1', '149.01700'),
('alcyone', NULL, '2013-02-21 10:19:33', '2017-07-12 11:42:50', 0, '4', '4', '1', '1', '156.25000'),
('andromeda', NULL, '2013-08-01 13:32:31', '2017-07-12 11:42:51', 0, '1', '1', '1', '1', '2.00000'),
('ASAv', NULL, '2015-02-05 10:56:15', '2016-12-19 14:34:19', 0, '1', '2', '1', '1', NULL),
('aspam-avir', NULL, '2012-11-24 23:59:59', '2017-07-12 11:55:59', 0, '4', '4', '1', '1', '10.00000'),
('bbb-ucnet', NULL, '2018-01-25 06:48:28', '2018-02-19 21:45:30', 0, '4', '4', '1', '1', '125.00000'),
('biotalent-65807', NULL, '2016-12-22 07:24:12', '2017-07-12 11:56:00', 0, '2', '2', '1', '1', '156.25000'),
('bs', NULL, '2013-03-11 07:20:08', '2017-12-03 20:34:20', 0, '4', '4', '1', '12', '12285.20000'),
('cardisoft-68014', NULL, '2017-12-14 07:33:41', '2018-02-19 21:45:35', 0, '4', '8', '1', '1', '0.00000'),
('cassiope', NULL, '2013-11-30 13:45:46', '2017-07-12 11:42:53', 0, '4', '8', '1', '1', '1862.65000'),
('ckan', NULL, '2013-06-03 04:59:49', '2017-07-12 11:56:04', 0, '2', '2', '1', '1', '0.00000'),
('ckan-CRETE', NULL, '2014-01-09 11:03:06', '2017-12-03 20:34:23', 0, '2', '2', '1', '1', '34.76560'),
('ckan-CY', NULL, '2012-11-24 23:59:59', '2016-12-19 14:34:19', 0, '2', '2', '1', '1', NULL),
('ckan-HER', NULL, '2013-11-19 12:47:33', '2017-07-12 11:42:54', 0, '2', '2', '1', '1', '34.76560'),
('cleano', NULL, '2013-02-21 08:05:50', '2017-07-12 11:42:55', 0, '4', '4', '1', '1', '156.25000'),
('cleano2', NULL, '2015-06-25 09:32:42', '2017-07-12 11:42:57', 0, '2', '2', '1', '1', '156.25000'),
('cleano3', NULL, '2016-01-28 13:34:30', '2017-07-12 11:56:10', 0, '2', '2', '1', '1', '205.07800'),
('csd-62224', NULL, '2015-09-15 06:12:24', '2017-07-12 11:56:11', 0, '2', '4', '1', '1', '97.65620'),
('csd-62422', NULL, '2015-10-02 13:32:08', '2017-07-12 11:56:12', 0, '2', '2', '1', '1', '48.82810'),
('csd-65191', NULL, '2016-10-14 12:34:01', '2017-07-12 11:56:14', 0, '2', '2', '1', '1', '58.59380'),
('csd-66376', NULL, '2017-03-17 06:28:26', '2017-12-03 20:34:35', 0, '4', '2', '1', '1', '48.82810'),
('csd-mta-64107', NULL, '2016-03-29 09:26:20', '2017-07-12 11:56:16', 0, '4', '8', '1', '1', '1536.00000'),
('csd-www', NULL, '2017-02-01 06:16:00', '2017-07-12 11:56:18', 0, '2', '4', '1', '1', '58.59380'),
('db-dimoi', NULL, '2012-11-24 23:59:59', '2018-02-19 21:38:22', 0, '1', '2', '1', '2', '1600.00000'),
('dbc1-61504-1', NULL, '2015-02-25 14:07:24', '2017-07-12 11:56:21', 0, '2', '2', '1', '2', '100.82800'),
('dbc1-61504-2', NULL, '2015-02-25 14:08:46', '2017-07-12 11:56:22', 0, '2', '2', '1', '2', '100.82800'),
('dbc1-61504-3', NULL, '2015-03-18 08:36:44', '2017-07-12 11:56:23', 0, '1', '1', '1', '1', '11.71880'),
('dbc1-61504-4', NULL, '2015-03-18 08:45:22', '2017-07-12 11:56:25', 0, '1', '1', '1', '1', '11.71880'),
('dbc1-61504-5', NULL, '2015-03-18 08:49:38', '2017-07-12 11:56:26', 0, '1', '1', '1', '1', '11.71880'),
('dbconnect', NULL, '2012-11-24 23:59:59', '2017-07-12 11:42:58', 0, '1', '1', '1', '1', '9.76562'),
('delos-59861', NULL, '2014-07-10 16:28:50', '2017-12-03 20:34:49', 0, '1', '4', '1', '2', '48.00000'),
('demo-windows-vlan800', NULL, '2012-11-24 23:59:59', '2017-07-12 11:56:30', 0, '2', '4', '1', '1', '9.76563'),
('dev-ked', NULL, '2013-08-01 06:15:21', '2017-07-12 11:43:00', 0, '2', '2', '1', '1', '156.25000'),
('dev-ked-win7', NULL, '2015-09-16 13:54:01', '2016-12-19 14:34:15', 0, '4', '8', '1', '1', NULL),
('dev4.tsl.gr_60053', NULL, '2014-07-17 05:53:45', '2017-07-12 11:56:33', 0, '2', '4', '1', '1', '97.65620'),
('ediamme-vm', NULL, '2012-11-24 23:59:59', '2018-02-19 21:46:06', 0, '2', '8', '1', '1', '0.00000'),
('elearn', NULL, '2013-07-03 06:30:34', '2017-07-12 11:43:02', 0, '4', '8', '1', '2', '2036.25000'),
('electra', NULL, '2013-10-24 09:51:28', '2017-07-12 11:56:37', 0, '2', '2', '1', '2', '546.80200'),
('ELKE_58805', NULL, '2014-06-18 07:23:39', '2017-07-12 11:56:38', 0, '2', '4', '1', '2', '937.65600'),
('ELKE_65026', NULL, '2016-10-03 09:45:44', '2017-07-12 11:56:40', 0, '2', '4', '1', '1', '64.00000'),
('ELKE_DB', NULL, '2013-08-05 06:10:58', '2017-07-12 11:43:04', 0, '4', '8', '1', '2', '133.13200'),
('ELLAK_SRV', NULL, '2014-05-08 06:05:30', '2017-07-12 11:56:42', 0, '8', '4', '1', '1', '781.25000'),
('ELLAK_SRV2', NULL, '2015-06-03 08:50:58', '2016-12-19 14:34:16', 0, '2', '2', '1', '1', NULL),
('ELLAK_SRV3', NULL, '2015-07-20 09:03:40', '2016-12-19 14:34:18', 0, '2', '2', '1', '1', NULL),
('ELLAK_vTHIN1', NULL, '2014-05-21 08:18:29', '2016-12-22 15:29:16', 0, '1', '', '1', '0', NULL),
('ELLAK_vTHIN2', NULL, '2014-05-21 10:28:03', '2016-12-22 15:30:47', 0, '1', '', '1', '0', NULL),
('ELLAK_vTHIN3', NULL, '2014-05-21 10:28:47', '2016-12-22 15:30:46', 0, '1', '', '1', '0', NULL),
('ELLAK_vTHIN4', NULL, '2014-06-06 10:55:35', '2016-12-22 15:30:50', 0, '1', '', '1', '0', NULL),
('ELLAK_vTHIN5', NULL, '2014-06-12 12:09:03', '2016-12-22 15:30:50', 0, '1', '', '1', '0', NULL),
('EMCESRS', NULL, '2014-06-20 13:04:46', '2017-12-03 20:35:02', 0, '2', '2', '1', '1', '64.00000'),
('eschool-med', NULL, '2015-07-24 06:50:08', '2017-07-12 11:43:05', 0, '4', '4', '1', '1', '100.00000'),
('fksserver', NULL, '2012-11-24 23:59:59', '2017-07-12 11:43:06', 0, '2', '8', '1', '1', '156.25000'),
('gunet-idp', NULL, '2015-04-02 12:18:33', '2016-12-19 14:34:17', 0, '2', '2', '1', '1', NULL),
('gunet-ldap-translucent', NULL, '2015-04-02 13:27:36', '2016-12-19 14:34:16', 0, '2', '2', '1', '1', NULL),
('gunet-mypassword', NULL, '2015-04-02 13:24:03', '2016-12-19 14:34:23', 0, '2', '2', '1', '1', NULL),
('gunet-sso-new', NULL, '2015-04-02 09:57:42', '2016-12-19 14:34:19', 0, '2', '2', '1', '1', NULL),
('gunet-uod', NULL, '2015-04-03 06:53:54', '2016-12-19 14:34:19', 0, '2', '4', '1', '1', NULL),
('gunet-uregister', NULL, '2015-04-02 13:14:15', '2016-12-19 14:34:18', 0, '2', '2', '1', '1', NULL),
('gunet-vd', NULL, '2015-04-03 06:50:55', '2016-12-19 14:34:13', 0, '2', '4', '1', '1', NULL),
('hack4med', NULL, '2012-11-24 23:59:59', '2016-12-19 14:34:20', 0, '1', '1', '1', '1', NULL),
('hpcstats-demo', NULL, '2012-11-24 23:59:59', '2017-07-12 11:56:46', 0, '2', '2', '1', '1', '0.00000'),
('ILYDA-66954', NULL, '2017-07-17 07:37:20', '2017-12-03 20:35:06', 0, '4', '4', '1', '1', '102.82800'),
('ILYDA-App1-61867', NULL, '2015-05-29 06:59:48', '2017-07-12 11:56:48', 0, '4', '8', '1', '1', '195.31200'),
('ILYDA-App2-62883', NULL, '2015-11-04 07:16:05', '2017-07-12 11:56:49', 0, '4', '4', '1', '1', '97.65620'),
('ILYDA-Db1-61867', NULL, '2015-06-03 07:50:58', '2016-12-22 15:23:19', 0, '4', '4', '1', '2', NULL),
('ILYDA-Db2-66477', NULL, '2017-03-31 05:50:15', '2018-02-19 21:46:20', 0, '4', '4', '1', '2', '0.00000'),
('ILYDA-Web1-61867', NULL, '2015-05-29 07:02:37', '2017-07-12 11:56:52', 0, '4', '4', '1', '1', '195.31200'),
('jpat-Win8', NULL, '2012-11-24 23:59:59', '2017-12-03 20:35:11', 0, '4', '2', '1', '1', '250.00000'),
('kalogerix', NULL, '2013-04-18 05:34:51', '2016-12-22 15:56:10', 0, '2', '2', '1', '1', NULL),
('knossos', NULL, '2013-08-20 08:03:29', '2017-07-12 11:43:08', 0, '1', '4', '1', '2', '307.37400'),
('knossos2', NULL, '2013-08-04 15:34:11', '2017-07-12 11:43:09', 0, '12', '16', '1', '0', '37.25290'),
('ldapdc', NULL, '2012-11-24 23:59:59', '2017-07-12 11:56:56', 0, '4', '2', '1', '1', '60.00000'),
('lib-67680-ejournals', NULL, '2017-11-03 09:20:57', '2017-12-03 20:35:17', 0, '2', '2', '1', '1', '198.82800'),
('libserver1', NULL, '2012-11-24 23:59:59', '2017-07-12 11:56:57', 0, '1', '4', '1', '1', '80.00000'),
('libserver2', NULL, '2012-11-24 23:59:59', '2018-04-20 10:54:16', 0, '2', '4', '1', '1', '80.00000'),
('libserver3-GIT', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:00', 0, '1', '2', '1', '1', '73.24220'),
('libserver5', NULL, '2013-05-16 10:20:27', '2017-07-12 11:57:01', 0, '4', '2', '1', '1', '39.06250'),
('lib_anemi', NULL, '2015-03-30 07:08:58', '2017-07-12 11:57:03', 0, '8', '16', '1', '2', '429.00000'),
('lib_e-locus', NULL, '2014-05-06 11:22:10', '2017-07-12 11:57:04', 0, '4', '4', '1', '1', '97.65620'),
('lib_soranos', NULL, '2014-06-02 18:27:57', '2017-07-12 11:57:05', 0, '2', '4', '1', '1', '250.00000'),
('loadbalancer', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:07', 0, '2', '2', '1', '1', '10.00000'),
('logserver', NULL, '2012-11-24 23:59:59', '2017-07-12 11:43:11', 0, '1', '1', '1', '1', '100.00000'),
('LTSP-math-66702', NULL, '2017-06-27 08:24:15', '2017-07-12 11:57:10', 0, '4', '8', '1', '1', '312.50000'),
('LTSP-math-66702-thin1', NULL, '2017-07-14 08:47:22', '2017-12-03 20:35:31', 0, '1', '', '1', '0', '0.00000'),
('LTSPSrv', NULL, '2013-05-28 05:53:04', '2017-07-12 11:43:12', 0, '2', '4', '1', '1', '156.25000'),
('mail-management', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:12', 0, '2', '2', '1', '1', '80.00000'),
('maillists', NULL, '2012-11-24 23:59:59', '2017-07-12 11:43:14', 0, '2', '1', '1', '1', '105.76600'),
('math-server', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:15', 0, '4', '16', '1', '1', '0.00000'),
('math-server2', NULL, '2013-10-21 08:37:54', '2017-07-12 11:43:16', 0, '2', '2', '1', '4', '2204.83000'),
('math-server3', NULL, '2013-11-01 10:29:51', '2017-12-03 20:35:38', 0, '4', '4', '1', '1', '308.82800'),
('math-server4-61439', NULL, '2015-03-23 12:19:14', '2017-07-12 11:57:19', 0, '1', '1', '1', '1', '23.43750'),
('med-59403', NULL, '2014-06-20 05:40:49', '2017-07-12 11:43:19', 0, '1', '1', '1', '1', '97.65620'),
('med-60162', NULL, '2014-07-30 05:42:14', '2017-07-12 11:43:21', 0, '2', '2', '1', '1', '78.12500'),
('med-hydra', NULL, '2017-10-20 10:43:23', '2017-12-03 20:35:43', 0, '2', '2', '1', '1', '60.00000'),
('mediawiki', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:23', 0, '2', '1', '1', '1', '19.53120'),
('med_57105', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:25', 0, '1', '2', '1', '1', '117.18800'),
('med_57885', NULL, '2013-11-15 09:17:14', '2017-07-12 11:43:22', 0, '2', '2', '1', '1', '48.82810'),
('merope', NULL, '2014-07-28 19:14:08', '2016-12-19 14:34:19', 0, '8', '2', '1', '1', NULL),
('mmlab_58600', NULL, '2013-12-23 07:18:31', '2017-07-12 11:57:27', 0, '2', '2', '1', '1', '48.82810'),
('mon', NULL, '2013-05-21 14:11:39', '2018-02-19 21:47:00', 0, '2', '2', '1', '1', '0.00000'),
('mta', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:30', 0, '2', '4', '1', '1', '120.00000'),
('nfssrv', NULL, '2014-01-22 20:05:55', '2018-04-20 10:54:51', 0, '8', '16', '1', '4', '11609.80000'),
('nhmc', NULL, '2012-11-24 23:59:59', '2017-07-12 11:57:33', 0, '2', '4', '1', '1', '0.00000'),
('nhmc-mobile_58851', NULL, '2014-02-12 09:30:29', '2017-07-12 11:57:34', 0, '2', '2', '1', '1', '48.82810'),
('nhmc_webGIS', NULL, '2012-11-24 23:59:59', '2017-12-03 20:35:56', 0, '2', '2', '1', '1', '48.82810'),
('Nikolaou-vm', NULL, '2012-11-24 23:59:59', '2018-04-20 10:45:51', 0, '2', '4', '1', '1', '160.00000'),
('nms_60930_A', NULL, '2014-11-13 06:59:08', '2017-07-12 11:57:39', 0, '4', '8', '1', '1', '39.06250'),
('nms_60930_B', NULL, '2014-11-20 07:54:00', '2017-07-12 11:57:40', 0, '1', '2', '1', '1', '103.06200'),
('nms_60930_E', NULL, '2014-11-25 10:58:43', '2017-07-12 11:57:41', 0, '1', '2', '1', '1', '103.06200'),
('old-cleano', NULL, '2015-06-02 18:37:55', '2017-07-12 11:57:43', 0, '2', '4', '1', '1', '186.26400'),
('oldeschool-med', NULL, '2015-07-21 08:22:28', '2017-07-12 11:57:44', 0, '1', '1', '1', '1', '20.00000'),
('om_ucnet_60551', NULL, '2014-10-01 06:23:42', '2016-12-19 14:34:23', 0, '4', '4', '1', '1', NULL),
('openeclass', NULL, '2012-11-24 23:59:59', '2017-07-12 11:43:23', 0, '2', '1', '1', '1', '48.82810'),
('OpenSIPS_2.1_VM', NULL, '2015-07-27 09:58:52', '2017-12-03 20:36:07', 0, '2', '2', '1', '1', '12.00000'),
('OTS_61366_app1', NULL, '2015-02-09 06:41:41', '2017-07-12 11:57:48', 0, '4', '4', '1', '1', '150.39100'),
('OTS_61366_app2', NULL, '2015-02-09 09:14:38', '2017-07-12 11:57:50', 0, '4', '20', '1', '1', '150.39100'),
('OTS_61366_db1', NULL, '2015-02-09 09:46:14', '2017-07-12 11:57:51', 0, '4', '4', '1', '1', '304.39100'),
('OTS_62427_web2', NULL, '2015-09-18 10:05:58', '2017-07-12 11:57:52', 0, '4', '4', '1', '1', '150.39100'),
('owncloud', NULL, '2012-11-24 23:59:59', '2016-12-19 14:34:22', 0, '2', '2', '1', '1', NULL),
('peir-gym-ira', NULL, '2012-11-24 23:59:59', '2017-07-12 11:43:25', 0, '2', '4', '1', '1', '156.25000'),
('PeriferiaDCtun-Srv', NULL, '2017-09-18 07:20:40', '2017-12-03 20:36:15', 0, '2', '2', '1', '1', '9.76562'),
('periferia_57530_app', NULL, '2013-05-17 05:38:51', '2017-07-12 11:57:55', 0, '8', '32', '1', '2', '1017.66000'),
('periferia_57530_db', NULL, '2013-05-17 06:12:08', '2017-07-12 11:57:57', 0, '8', '32', '1', '2', '488.28100'),
('periferia_59070_ubuntu', NULL, '2014-02-27 09:48:15', '2017-07-12 11:57:58', 0, '4', '8', '1', '3', '608.28100'),
('periferia_64574_app', NULL, '2016-06-22 10:26:37', '2017-07-12 11:57:59', 0, '4', '10', '1', '1', '208.82800'),
('periferia_64574_db', NULL, '2016-06-22 10:29:29', '2018-02-19 21:47:30', 0, '4', '10', '1', '3', '1423.66000'),
('periferia_65714_vhosting', NULL, '2016-12-06 11:08:53', '2017-07-12 11:58:02', 0, '2', '2', '1', '1', '156.25000'),
('periferia_ekloges_admin1', NULL, '2017-07-04 05:51:41', '2017-07-12 11:58:04', 0, '2', '2', '1', '1', '156.25000'),
('periferia_ekloges_hadb', NULL, '2017-06-21 08:21:36', '2017-07-12 11:58:05', 0, '2', '2', '1', '1', '4.88281'),
('periferia_ekloges_hapublic', NULL, '2017-07-03 06:06:38', '2017-07-12 11:58:06', 0, '2', '2', '1', '1', '11.71880'),
('periferia_ekloges_mdb01', NULL, '2017-06-14 07:40:34', '2017-07-12 11:58:08', 0, '2', '2', '1', '1', '78.12500'),
('periferia_ekloges_mdb02', NULL, '2017-06-14 07:49:53', '2017-07-12 11:58:09', 0, '2', '2', '1', '1', '78.12500'),
('periferia_ekloges_mdb03', NULL, '2017-06-14 07:55:10', '2017-07-12 11:58:10', 0, '2', '2', '1', '1', '78.12500'),
('periferia_ekloges_web1', NULL, '2017-07-03 06:31:46', '2018-02-19 21:47:41', 0, '4', '4', '1', '1', '0.00000'),
('periferia_ekloges_web2', NULL, '2017-07-04 05:15:03', '2017-07-12 11:58:13', 0, '4', '4', '1', '1', '156.25000'),
('periferia_papyros', NULL, '2015-10-09 06:55:20', '2017-06-01 08:32:11', 0, '2', '8', '1', '1', NULL),
('Phl1_Reth', NULL, '2012-11-24 23:59:59', '2018-04-20 10:46:06', 0, '2', '4', '1', '1', '78.12500'),
('plex_57141', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:16', 0, '4', '8', '1', '1', '156.25000'),
('pop-imap', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:17', 0, '12', '16', '1', '1', '100.00000'),
('PTDE_66588', NULL, '2017-04-28 10:33:40', '2017-07-12 11:58:18', 0, '4', '4', '1', '1', '195.31200'),
('radius1', NULL, '2014-03-20 07:06:03', '2017-07-12 11:43:26', 0, '2', '2', '1', '2', '0.00000'),
('radius2', NULL, '2016-09-29 11:46:02', '2017-07-12 11:58:21', 0, '2', '2', '1', '1', '93.75000'),
('seafile-srv', NULL, '2015-09-17 12:47:13', '2017-07-12 11:58:22', 0, '2', '2', '1', '1', '166.01600'),
('secr-db-1', NULL, '2015-05-13 13:54:31', '2017-07-12 11:58:24', 0, '24', '16', '1', '2', '700.00000'),
('secr-rports-1', NULL, '2015-05-14 08:32:36', '2017-07-12 11:58:25', 0, '1', '2', '1', '1', '55.00000'),
('secr-web-1', NULL, '2015-05-13 13:44:58', '2017-07-12 11:58:27', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-2', NULL, '2015-09-23 07:10:43', '2017-12-03 20:36:48', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-3', NULL, '2015-09-23 07:18:35', '2017-12-03 20:36:50', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-4', NULL, '2015-09-23 07:20:28', '2017-12-03 20:36:51', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-5', NULL, '2015-09-23 08:07:03', '2017-12-03 20:36:53', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-6', NULL, '2015-09-23 08:08:57', '2017-12-03 20:36:54', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-7', NULL, '2015-09-23 08:16:20', '2017-12-03 20:36:56', 0, '4', '4', '1', '1', '71.00000'),
('secr-web-lb', NULL, '2015-09-23 08:52:11', '2017-12-03 20:36:57', 0, '2', '2', '1', '1', '19.53120'),
('secrapp-class-1', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:28', 0, '2', '2', '1', '1', '105.00000'),
('secrapp-stdnt-1', NULL, '2015-05-13 13:04:53', '2017-07-12 11:58:29', 0, '8', '8', '1', '1', '105.00000'),
('secrweb-profs-1', NULL, '2015-05-14 08:30:10', '2017-12-03 20:37:01', 0, '1', '2', '1', '1', '109.00000'),
('shibboleth', NULL, '2013-07-11 13:02:09', '2017-07-12 11:43:27', 0, '4', '2', '1', '1', '9.31322'),
('sites-vits', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:33', 0, '8', '8', '1', '1', '976.56200'),
('soc-66915', NULL, '2017-06-29 08:41:48', '2017-07-12 11:58:35', 0, '4', '4', '1', '1', '195.31200'),
('sso', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:36', 0, '2', '1', '1', '1', '10.00000'),
('stats-ucnet', NULL, '2014-07-28 06:04:25', '2016-12-19 14:34:19', 0, '1', '', '1', '1', NULL),
('stirizo-devel2', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:37', 0, '8', '4', '1', '1', '0.00000'),
('stirizo-devel2-PSD', NULL, '2012-11-24 23:59:59', '2018-04-20 10:56:07', 0, '12', '8', '1', '2', '308.82800'),
('stirizo-devel3', NULL, '2012-11-24 23:59:59', '2016-12-19 14:34:18', 0, '2', '2', '1', '1', NULL),
('stirizo-devel3-PSD', NULL, '2013-08-05 13:44:51', '2016-12-19 14:34:22', 0, '4', '8', '1', '1', NULL),
('synnefo-node1', NULL, '2016-10-17 11:00:59', '2017-07-12 11:58:40', 0, '2', '2', '1', '1', '62.50000'),
('synnefo-node2', NULL, '2016-10-17 11:02:29', '2017-12-03 20:37:12', 0, '2', '2', '1', '1', '62.50000'),
('tacacs', NULL, '2015-11-20 10:49:11', '2017-07-12 11:58:43', 0, '2', '2', '1', '1', '23.43750'),
('telesforos', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:44', 0, '2', '1', '1', '1', '59.29690'),
('test-VM-jpat-32G', NULL, '2015-03-11 14:25:29', '2017-04-25 12:09:47', 0, '1', '2', '1', '7', NULL),
('test_vlan15', NULL, '2013-08-01 12:31:06', '2016-12-22 15:23:14', 0, '2', '2', '1', '7', NULL),
('tety-64515', NULL, '2016-06-08 08:46:58', '2017-07-12 11:43:29', 0, '4', '4', '1', '2', '800.82800'),
('tety-s2', NULL, '2013-11-01 11:37:41', '2018-02-19 21:48:13', 0, '2', '2', '1', '2', '0.00000'),
('tety-s3', NULL, '2014-06-16 09:24:00', '2017-07-12 11:58:48', 0, '2', '4', '1', '2', '548.82800'),
('tetyserver1', NULL, '2012-11-24 23:59:59', '2017-07-12 11:58:50', 0, '1', '4', '1', '1', '80.00000'),
('tety_s4_64733', NULL, '2016-07-29 06:16:36', '2017-07-12 11:58:51', 0, '2', '4', '1', '1', '48.82810'),
('TicketID_60624', NULL, '2014-12-22 13:10:26', '2017-07-12 11:58:52', 0, '4', '4', '1', '1', '195.31200'),
('TicketID_64645', NULL, '2016-07-25 05:42:42', '2017-07-12 11:58:54', 0, '4', '8', '1', '1', '375.00000'),
('TicketID_64770_1', NULL, '2016-08-09 07:53:07', '2016-12-19 14:34:13', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_2', NULL, '2016-08-09 08:06:50', '2016-12-19 14:34:21', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_2_m', NULL, '2016-08-09 09:41:46', '2016-12-19 14:34:16', 0, '2', '4', '1', '1', NULL),
('TicketID_64770_3', NULL, '2016-08-09 08:25:46', '2016-12-19 14:34:13', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_3_m', NULL, '2016-08-09 09:39:02', '2016-12-19 14:34:23', 0, '2', '4', '1', '1', NULL),
('TicketID_64770_4', NULL, '2016-08-10 20:35:15', '2016-12-19 14:34:23', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_4_m', NULL, '2016-08-10 20:38:29', '2016-12-19 14:34:20', 0, '2', '4', '1', '1', NULL),
('TicketID_64770_5', NULL, '2016-08-10 20:44:59', '2016-12-19 14:34:15', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_5_m', NULL, '2016-08-10 20:46:41', '2016-12-19 14:34:15', 0, '2', '4', '1', '1', NULL),
('TicketID_64770_6', NULL, '2016-08-10 21:02:25', '2016-12-19 14:34:14', 0, '4', '8', '1', '1', NULL),
('TicketID_64770_6_m', NULL, '2016-08-10 21:08:03', '2016-12-19 14:34:16', 0, '2', '4', '1', '1', NULL),
('TicketID_65409_lib', NULL, '2016-11-16 08:12:44', '2017-07-12 11:58:55', 0, '2', '2', '1', '1', '80.00000'),
('tmp-kaltura', NULL, '2013-10-01 08:45:34', '2016-12-19 14:34:14', 0, '4', '4', '1', '1', NULL),
('tmp_59749', NULL, '2014-06-03 05:16:07', '2017-07-12 11:58:57', 0, '1', '2', '1', '1', '150.00000'),
('tms-vc-int', NULL, '2013-11-19 10:56:16', '2017-07-12 11:43:32', 0, '4', '4', '1', '1', '117.18800'),
('tolis_59192', NULL, '2014-03-17 07:03:04', '2017-07-12 11:58:59', 0, '4', '8', '1', '1', '97.65620'),
('torps', NULL, '2014-04-02 05:52:49', '2018-02-19 21:48:24', 0, '2', '2', '1', '1', '0.00000'),
('tyserv', NULL, '2013-08-04 20:53:24', '2017-07-12 11:43:34', 0, '2', '1', '1', '1', '186.26500'),
('UCnetDCtun', NULL, '2015-05-26 20:25:00', '2016-12-19 14:34:21', 0, '2', '1', '1', '1', NULL),
('ultivpn', NULL, '2018-01-16 09:05:52', '2018-02-19 21:48:26', 0, '2', '2', '1', '1', '48.82810'),
('vc-int', NULL, '2013-11-19 06:46:13', '2017-07-12 11:43:36', 0, '2', '2', '1', '1', '48.82810'),
('VDi-1-prytaneia', NULL, '2014-01-20 09:43:24', '2016-12-19 14:34:23', 0, '2', '2', '1', '1', NULL),
('velos', NULL, '2013-08-07 11:48:00', '2017-07-12 11:43:37', 0, '8', '8', '1', '1', '93.13220'),
('vhyp1', NULL, '2015-01-26 09:06:00', '2016-12-19 14:34:18', 0, '4', '4', '1', '1', NULL),
('vod-telesforos', NULL, '2014-01-28 06:41:19', '2016-12-22 15:23:09', 0, '4', '4', '1', '2', NULL),
('vod-ucnet', NULL, '2014-07-24 06:52:19', '2017-07-12 11:59:06', 0, '2', '2', '1', '3', '234.37500'),
('voip-proxy', NULL, '2013-08-02 09:01:08', '2017-07-12 11:43:38', 0, '1', '1', '1', '1', '5.00000'),
('VoipMgmtsrv', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:09', 0, '2', '2', '1', '1', '9.76562'),
('VoipServer-NNI1', NULL, '2015-02-13 09:31:19', '2017-07-12 11:59:10', 0, '2', '2', '1', '1', '62.50000'),
('VoipServer2', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:12', 0, '2', '2', '1', '1', '9.76562'),
('VoipServer3', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:13', 0, '4', '4', '1', '1', '9.76562'),
('VoipServer4', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:14', 0, '4', '4', '1', '1', '9.76562'),
('VoipServer5-SBC1', NULL, '2014-06-26 10:03:09', '2017-07-12 11:59:16', 0, '2', '2', '1', '1', '62.50000'),
('VoipServer6-X11', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:17', 0, '2', '2', '1', '1', '32.00000'),
('VoipServer7', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:18', 0, '2', '1', '1', '1', '9.76562'),
('VoipServer8', NULL, '2012-11-24 23:59:59', '2016-12-19 14:34:16', 0, '8', '4', '1', '1', NULL),
('VoipServer8_112', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:20', 0, '8', '4', '1', '1', '59.76560'),
('VoipServer_fserv', NULL, '2016-08-31 11:43:49', '2016-12-19 14:34:17', 0, '2', '4', '1', '1', NULL),
('VoipServer_Hylafax_Server', NULL, '2015-09-24 09:41:33', '2016-12-19 14:34:19', 0, '2', '2', '1', '1', NULL),
('VoipServer_MariaDB_node1', NULL, '2015-08-19 07:10:39', '2017-07-12 11:59:21', 0, '2', '2', '1', '1', '19.53120'),
('VoipServer_MariaDB_node2', NULL, '2015-08-19 07:13:59', '2017-07-12 11:59:22', 0, '4', '2', '1', '1', '19.53120'),
('vpnserver2', NULL, '2013-11-20 10:30:25', '2017-07-12 11:59:24', 0, '2', '2', '1', '1', '5.00000'),
('wcs-manager', NULL, '2013-08-02 07:46:05', '2016-12-19 14:34:23', 0, '2', '2', '1', '1', NULL),
('web', NULL, '2012-11-24 23:59:59', '2017-07-12 11:59:25', 0, '8', '16', '1', '2', '0.00000'),
('Win7_THIN_DEMO', NULL, '2013-05-21 05:38:24', '2017-07-12 11:59:27', 0, '2', '2', '1', '1', '39.06250'),
('wowza-ucnet', NULL, '2014-08-04 06:56:46', '2017-07-12 11:43:40', 0, '4', '8', '1', '1', '507.81200'),
('www_ucnet', NULL, '2014-06-23 06:55:10', '2017-07-12 11:59:29', 0, '2', '2', '1', '1', '39.06250'),
('xalkis-61656-fs', NULL, '2015-04-23 09:18:08', '2017-07-12 11:59:31', 0, '2', '2', '1', '4', '2088.83000'),
('xalkis-62930-Win7', NULL, '2015-10-26 10:07:06', '2017-07-12 11:59:32', 0, '2', '4', '1', '1', '256.00000'),
('xalkis-65677', NULL, '2016-12-14 10:06:12', '2017-07-12 11:59:33', 0, '4', '4', '1', '1', '390.62500'),
('xalkis-66797', NULL, '2017-05-29 12:20:07', '2017-12-03 20:27:04', 0, '8', '8', '1', '1', '390.62500'),
('xalkis-66910', NULL, '2017-06-26 11:35:46', '2017-07-12 11:59:36', 0, '8', '8', '1', '1', '390.62500'),
('xenios', NULL, '2013-02-22 10:48:06', '2017-07-12 11:43:41', 0, '4', '4', '1', '1', '156.25000');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `vmNetwork`
--

CREATE TABLE `vmNetwork` (
  `vmName` varchar(64) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FQDN` varchar(254) DEFAULT NULL,
  `VlanID` varchar(64) NOT NULL DEFAULT '820',
  `IPAddress` varchar(254) NOT NULL DEFAULT 'NULL',
  `MAC` varchar(254) NOT NULL DEFAULT 'NULL',
  `Netmask` varchar(254) DEFAULT NULL,
  `GW` varchar(254) DEFAULT NULL,
  `DNS1` varchar(254) DEFAULT NULL,
  `DNS2` varchar(254) DEFAULT NULL,
  `ipV` enum('4','6') NOT NULL DEFAULT '4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Άδειασμα δεδομένων του πίνακα `vmNetwork`
--

INSERT INTO `vmNetwork` (`vmName`, `modificationDate`, `FQDN`, `VlanID`, `IPAddress`, `MAC`, `Netmask`, `GW`, `DNS1`, `DNS2`, `ipV`) VALUES
('A2-pm-60563', '2014-10-02 05:40:11', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('A2-pm-60563-dev', '2015-03-18 14:22:54', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('aeahk', '2012-11-24 23:59:59', 'aeahk.datacenter.uoc.gr', '820', '147.52.82.115', '00:16:3E:09:EA:D8', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('aheevaacs', '2016-05-01 09:17:44', '', '820', '147.52.82.112', '00:24:81:FD:3E:F0', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('alcyone', '2013-02-21 10:19:33', 'alcyone.datacenter.uoc.gr', '825', '147.52.82.195', '00:16:3E:27:31:F1', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('andromeda', '2013-08-01 13:32:31', 'andromeda.ucnet.uoc.gr', '15', '147.52.80.16', '00:16:3E:63:8E:F8', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('ASAv', '2015-02-05 10:56:15', NULL, '450', 'NULL', 'NULL', NULL, NULL, NULL, NULL, '4'),
('aspam-avir', '2012-11-24 23:59:59', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('bbb-ucnet', '2018-01-25 06:48:28', '3b-ucnet.datacenter.uoc.gr', '825', '147.52.82.180', '00:16:3E:5E:9C:96', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('biotalent-65807', '2016-12-22 07:24:12', 'biotalent.ucdc.uoc.gr', '820', '147.52.82.82', '00:16:3E:36:D5:AC', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('bs', '2013-03-11 07:20:08', 'bs.datacenter.uoc.gr', '825', '147.52.82.200', '00:16:3E:07:5C:90', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('bs', '2013-03-11 07:20:08', 'fs.datacenter.uoc.gr', '825', '147.52.82.201', '00:16:3E:07:5C:90', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('bs', '2013-03-11 07:20:08', '', '2561', '192.168.19.250', '00:16:3E:5D:4B:B5', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('bs', '2013-03-11 07:20:08', '', '855', '147.52.200.46', '00:16:3E:7D:38:8F', '255.255.255.240', '147.52.200.35', NULL, NULL, '4'),
('bs', '2013-03-11 07:20:08', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('cardisoft-68014', '2017-12-14 07:33:41', '', '869', '147.52.200.250', '00:16:3E:70:79:31', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('cassiope', '2013-11-30 13:45:46', 'ucnetnt.ucnet.uoc.gr', '825', '147.52.82.153', '00:16:3E:63:8E:E6', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('cassiope', '2013-11-30 13:45:46', 'cassiope.lan.ucnet.uoc.gr', '825', '147.52.82.159', '00:16:3E:63:8E:E6', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ckan', '2013-06-03 04:59:49', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ckan-CRETE', '2014-01-09 11:03:06', 'ckan-67709.datacenter.uoc.gr', '820', '147.52.82.77', '00:16:3E:4A:C1:8C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ckan-CRETE', '2014-01-09 11:03:06', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ckan-CY', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ckan-HER', '2013-11-19 12:47:33', 'heraklionopencity.datacenter.uoc.gr', '820', '147.52.82.78', '00:16:3E:16:E2:47', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('cleano', '2013-02-21 08:05:50', 'cleano.datacenter.uoc.gr', '825', '147.52.82.185', '00:16:3E:1B:0F:14', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('cleano2', '2015-06-25 09:32:42', 'cleano2.datacenter.uoc.gr', '825', '147.52.82.188', '00:16:3E:2E:45:99', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('cleano3', '2016-01-28 13:34:30', 'cleano3.datacenter.uoc.gr', '825', '147.52.82.189', '00:16:3E:6D:8E:D5', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('csd-62224', '2015-09-15 06:12:24', 'csd-62224.datacenter.uoc.gr', '820', '147.52.82.63', '00:16:3E:2E:1C:70', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-62422', '2015-10-02 13:32:08', 'csd-62422.datacenter.uoc.gr', '820', '147.52.82.111', '00:16:3E:3E:3E:B5', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-62422', '2015-10-02 13:32:08', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.1', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'mta1.datacenter.uoc.gr', '820', '147.52.82.101', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'mta3.datacenter.uoc.gr', '820', '147.52.82.105', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'mta4.datacenter.uoc.gr', '820', '147.52.82.106', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'csd-vpn.datacenter.uoc.gr', '820', '147.52.82.109', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'cb2-2.datacenter.uoc.gr', '820', '147.52.82.22', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.38', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.40', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'ejournals.lib.uoc.gr', '820', '147.52.82.49', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.53', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.58', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'ckan-67709.datacenter.uoc.gr', '820', '147.52.82.77', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.8', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', 'opendata.datacenter.uoc.gr', '820', '147.52.82.81', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-65191', '2016-10-14 12:34:01', '', '820', '147.52.82.86', '00:16:3E:4B:5C:87', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-66376', '2017-03-17 06:28:26', 'nextcloud.datacenter.uoc.gr', '880', '147.52.204.28', '00:16:3E:32:AB:C5', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('csd-mta-64107', '2016-03-29 09:26:20', 'tenar.csd.uoc.gr', '820', '147.52.82.34', '00:16:3E:0C:3E:8D', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('csd-www', '2017-02-01 06:16:00', 'tehanu.csd.uoc.gr', '820', '147.52.82.108', '00:16:3E:48:77:23', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('db-dimoi', '2012-11-24 23:59:59', '', '820', '147.52.82.55', '00:16:3E:2E:F3:F0', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('dbc1-61504-1', '2015-02-25 14:07:24', 'dbc1-61504-1.datacenter.uoc.gr', '865', '147.52.200.101', '00:16:3E:35:C8:6D', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbc1-61504-2', '2015-02-25 14:08:46', 'dbc1-61504-2.datacenter.uoc.gr', '865', '147.52.200.102', '00:16:3E:48:DF:A9', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbc1-61504-3', '2015-03-18 08:36:44', 'dbc1-61504-3.datacenter.uoc.gr', '865', '147.52.200.103', '00:16:3E:5B:AC:3E', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbc1-61504-4', '2015-03-18 08:45:22', 'dbc1-61504-4.datacenter.uoc.gr', '865', '147.52.200.104', '00:16:3E:42:37:63', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbc1-61504-4', '2015-03-18 08:45:22', '', '865', '147.52.200.106', '00:16:3E:42:37:63', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbc1-61504-5', '2015-03-18 08:49:38', 'dbc1-61504-5.datacenter.uoc.gr', '865', '147.52.200.105', '00:16:3E:5A:7A:2D', '255.255.255.240', '147.52.200.100', NULL, NULL, '4'),
('dbconnect', '2012-11-24 23:59:59', '', '825', '147.52.82.130', '00:16:3E:5E:AC:E9', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('delos-59861', '2014-07-10 16:28:50', 'delos.datacenter.uoc.gr', '820', '147.52.82.114', '00:16:3E:3E:EF:3C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('demo-windows-vlan800', '2012-11-24 23:59:59', NULL, '800', 'NULL', 'NULL', NULL, NULL, NULL, NULL, '4'),
('dev-ked', '2013-08-01 06:15:21', 'dev.datacenter.uoc.gr', '825', '147.52.82.199', '00:16:3E:2D:4D:79', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('dev-ked-win7', '2015-09-16 13:54:01', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('dev4.tsl.gr_60053', '2014-07-17 05:53:45', 'dev4.tsl.gr', '820', '147.52.82.62', '00:16:3E:24:78:6C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ediamme-vm', '2012-11-24 23:59:59', 'elearning.edc.uoc.gr', '820', '147.52.82.47', '00:16:3E:7F:57:39', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('elearn', '2013-07-03 06:30:34', 'elearn.datacenter.uoc.gr', '825', '147.52.82.186', '00:16:3E:0F:FF:94', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('electra', '2013-10-24 09:51:28', '', '15', '147.52.80.15', '00:16:3E:3E:2C:0C', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('electra', '2013-10-24 09:51:28', 'electra.ucnet.uoc.gr', '825', '147.52.82.215', '00:16:3E:58:9D:60', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('electra', '2013-10-24 09:51:28', 'taygete.ucnet.uoc.gr', '825', '147.52.82.216', '00:16:3E:58:9D:60', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ELKE_58805', '2014-06-18 07:23:39', '', '855', '147.52.200.36', '00:16:3E:5A:35:09', '255.255.255.240', '147.52.200.35', NULL, NULL, '4'),
('ELKE_65026', '2016-10-03 09:45:44', '', '855', '147.52.200.37', '00:16:3E:4C:8D:44', '255.255.255.240', '147.52.200.35', NULL, NULL, '4'),
('ELKE_DB', '2013-08-05 06:10:58', '', '15', '147.52.80.41', '52:54:00:12:34:56', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('ELLAK_SRV', '2014-05-08 06:05:30', 'ellaksrv.datacenter.uoc.gr', '825', '147.52.82.170', '00:16:3E:0A:D3:C9', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ELLAK_SRV', '2014-05-08 06:05:30', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ELLAK_SRV2', '2015-06-03 08:50:58', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ELLAK_SRV3', '2015-07-20 09:03:40', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ELLAK_vTHIN1', '2014-05-21 08:18:29', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('ELLAK_vTHIN2', '2014-05-21 10:28:03', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('ELLAK_vTHIN3', '2014-05-21 10:28:47', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('ELLAK_vTHIN4', '2014-06-06 10:55:35', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('ELLAK_vTHIN5', '2014-06-12 12:09:03', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('EMCESRS', '2014-06-20 13:04:46', '', '825', '147.52.82.160', '00:16:3E:09:AE:15', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('EMCESRS', '2014-06-20 13:04:46', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('eschool-med', '2015-07-24 06:50:08', 'eschool.med.uoc.gr', '820', '147.52.82.30', '52:54:00:1A:B9:9F', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('fksserver', '2012-11-24 23:59:59', 'vm.fks.uoc.gr', '820', '147.52.82.42', '00:16:3E:5C:9D:F4', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('gunet-idp', '2015-04-02 12:18:33', '', '875', '147.52.81.56', '00:16:3E:1A:B8:AE', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-idp', '2015-04-02 12:18:33', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-ldap-translucent', '2015-04-02 13:27:36', '', '875', '147.52.81.59', '00:16:3E:7C:35:6B', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-ldap-translucent', '2015-04-02 13:27:36', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-mypassword', '2015-04-02 13:24:03', 'mypassword.uoc.gr', '875', '147.52.81.58', '00:16:3E:6D:40:90', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-mypassword', '2015-04-02 13:24:03', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-sso-new', '2015-04-02 09:57:42', '', '875', '147.52.81.55', '00:16:3E:6F:2D:3B', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-sso-new', '2015-04-02 09:57:42', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-uod', '2015-04-03 06:53:54', 'authn.uoc.gr', '875', '147.52.81.53', '00:16:3E:54:56:11', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-uod', '2015-04-03 06:53:54', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-uregister', '2015-04-02 13:14:15', 'uregister.uoc.gr', '875', '147.52.81.57', '00:16:3E:62:04:FC', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-uregister', '2015-04-02 13:14:15', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-vd', '2015-04-03 06:50:55', 'vd.uoc.gr', '875', '147.52.81.54', '00:16:3E:25:D4:CE', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('gunet-vd', '2015-04-03 06:50:55', NULL, '875', 'NULL', 'NULL', '255.255.255.224', '147.52.81.36', NULL, NULL, '4'),
('hack4med', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('hpcstats-demo', '2012-11-24 23:59:59', 'hpcstats.datacenter.uoc.gr', '825', '147.52.82.225', '00:16:3E:75:F1:68', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('hpcstats-demo', '2012-11-24 23:59:59', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('ILYDA-66954', '2017-07-17 07:37:20', '', '872', '147.52.200.201', '00:16:3E:21:BF:78', '255.255.255.240', '147.52.200.196', NULL, NULL, '4'),
('ILYDA-App1-61867', '2015-05-29 06:59:48', 'student-admin.cict.uoc.gr', '871', '147.52.200.184', '00:16:3E:28:D2:0C', '255.255.255.240', '147.52.200.180', NULL, NULL, '4'),
('ILYDA-App1-61867', '2015-05-29 06:59:48', NULL, '871', 'NULL', 'NULL', '255.255.255.240', '147.52.200.180', NULL, NULL, '4'),
('ILYDA-App2-62883', '2015-11-04 07:16:05', NULL, '871', 'NULL', 'NULL', '255.255.255.240', '147.52.200.180', NULL, NULL, '4'),
('ILYDA-Db1-61867', '2015-06-03 07:50:58', NULL, '870', 'NULL', 'NULL', '255.255.255.240', '147.52.200.164', NULL, NULL, '4'),
('ILYDA-Db2-66477', '2017-03-31 05:50:15', '', '870', '147.52.200.168', '00:16:3E:17:9E:CA', '255.255.255.240', '147.52.200.164', NULL, NULL, '4'),
('ILYDA-Db2-66477', '2017-03-31 05:50:15', NULL, '870', 'NULL', 'NULL', '255.255.255.240', '147.52.200.164', NULL, NULL, '4'),
('ILYDA-Web1-61867', '2015-05-29 07:02:37', 'student.cict.uoc.gr', '872', '147.52.200.200', '00:16:3E:6F:7A:C4', '255.255.255.240', '147.52.200.196', NULL, NULL, '4'),
('ILYDA-Web1-61867', '2015-05-29 07:02:37', NULL, '872', 'NULL', 'NULL', '255.255.255.240', '147.52.200.196', NULL, NULL, '4'),
('jpat-Win8', '2012-11-24 23:59:59', 'jpat-dev3.ucnet.uoc.gr', '820', '147.52.82.99', '00:16:3E:5A:F6:CA', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('jpat-Win8', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('kalogerix', '2013-04-18 05:34:51', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('knossos', '2013-08-20 08:03:29', 'knossos.ucnet.uoc.gr', '15', '147.52.80.3', '00:0F:20:F6:FF:4F', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('ldapdc', '2012-11-24 23:59:59', 'ldapdc.datacenter.uoc.gr', '820', '147.52.82.60', '00:16:3E:51:7B:8C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('lib-67680-ejournals', '2017-11-03 09:20:57', 'ejournals.lib.uoc.gr', '820', '147.52.82.49', '00:16:3E:7D:09:39', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('libserver1', '2012-11-24 23:59:59', 'vm-zilla.lib.uoc.gr', '820', '147.52.82.43', '00:16:3E:44:02:86', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('libserver2', '2012-11-24 23:59:59', '', '820', '147.52.82.44', '00:16:3E:5E:AE:27', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('libserver3-GIT', '2012-11-24 23:59:59', 'gitlab.lib.uoc.gr', '820', '147.52.82.39', '00:16:3E:34:5A:4C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('libserver5', '2013-05-16 10:20:27', '', '820', '147.52.82.35', '00:16:3E:27:13:07', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('lib_anemi', '2015-03-30 07:08:58', 'anemi.datacenter.uoc.gr', '866', '147.52.200.135', '00:16:3E:2D:BE:EB', '255.255.255.224', '147.52.200.132', NULL, NULL, '4'),
('lib_e-locus', '2014-05-06 11:22:10', 'e-locus.datacenter.uoc.gr', '820', '147.52.82.118', '00:16:3E:51:6D:5A', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('lib_soranos', '2014-06-02 18:27:57', 'soranos.datacenter.uoc.gr', '820', '147.52.82.80', '00:16:3E:1E:9E:74', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('loadbalancer', '2012-11-24 23:59:59', '', '820', '147.52.82.100', '00:16:3E:15:E8:88', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('loadbalancer', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('logserver', '2012-11-24 23:59:59', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('LTSP-math-66702', '2017-06-27 08:24:15', 'lab-server.math.uoc.gr', '825', '147.52.82.173', '00:16:3E:01:BA:60', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('LTSP-math-66702', '2017-06-27 08:24:15', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('LTSP-math-66702-thin1', '2017-07-14 08:47:22', NULL, 'virbr', 'NULL', 'NULL', '255.255.255.0', '192.168.122.1', NULL, NULL, '4'),
('LTSPSrv', '2013-05-28 05:53:04', 'dcsrv.datacenter.uoc.gr', '825', '147.52.82.210', '00:16:3E:19:59:73', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('LTSPSrv', '2013-05-28 05:53:04', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('mail-management', '2012-11-24 23:59:59', '', '820', '147.52.82.102', '00:16:3E:7E:F7:C4', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mail-management', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('maillists', '2012-11-24 23:59:59', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('math-server', '2012-11-24 23:59:59', 'euclid.math.uoc.gr', '820', '147.52.82.50', '00:16:3E:0E:92:0E', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('math-server2', '2013-10-21 08:37:54', 'pythagoras.math.uoc.gr', '820', '147.52.82.71', '00:16:3E:4F:B7:1B', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('math-server3', '2013-11-01 10:29:51', 'polygon.math.uoc.gr', '820', '147.52.82.72', '00:16:3E:5C:EC:0B', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('math-server4-61439', '2015-03-23 12:19:14', '', '820', '147.52.82.113', '00:16:3E:5D:1C:3F', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('med-59403', '2014-06-20 05:40:49', 'med-59403.datacenter.uoc.gr', '820', '147.52.82.95', '00:16:3E:7E:2E:92', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('med-60162', '2014-07-30 05:42:14', 'med-60162.datacenter.uoc.gr', '820', '147.52.82.110', '00:16:3E:75:29:76', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('med-hydra', '2017-10-20 10:43:23', '', '825', '147.52.82.144', '00:16:3E:45:22:78', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('mediawiki', '2012-11-24 23:59:59', 'wiki.datacenter.uoc.gr', '820', '147.52.82.67', '00:16:3E:59:D5:D3', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('med_57105', '2012-11-24 23:59:59', 'cpmn.datacenter.uoc.gr', '820', '147.52.82.69', '00:16:3E:59:F2:95', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('med_57885', '2013-11-15 09:17:14', 'med-57885.datacenter.uoc.gr', '820', '147.52.82.75', '00:16:3E:19:91:DC', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('merope', '2014-07-28 19:14:08', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mmlab_58600', '2013-12-23 07:18:31', 'mmlab-58600.datacenter.uoc.gr', '820', '147.52.82.76', '00:16:3E:0F:D2:A7', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mmlab_58600', '2013-12-23 07:18:31', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mon', '2013-05-21 14:11:39', 'mon.datacenter.uoc.gr', '825', '147.52.82.150', '00:16:3E:40:5B:DB', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('mon', '2013-05-21 14:11:39', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('mta', '2012-11-24 23:59:59', 'mta.datacenter.uoc.gr', '820', '147.52.82.103', '00:16:3E:7A:DF:A7', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mta', '2012-11-24 23:59:59', 'mta2.datacenter.uoc.gr', '820', '147.52.82.104', '00:16:3E:7A:DF:A7', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('mta', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('nfssrv', '2014-01-22 20:05:55', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('nhmc', '2012-11-24 23:59:59', '', '820', '147.52.82.41', '00:16:3E:74:64:EA', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('nhmc-mobile_58851', '2014-02-12 09:30:29', 'mobile.datacenter.uoc.gr', '820', '147.52.82.68', '00:16:3E:27:A8:FC', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('nhmc_webGIS', '2012-11-24 23:59:59', 'nhmc-webGIS.datacenter.uoc.gr', '820', '147.52.82.97', '00:16:3E:6D:D0:4C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('Nikolaou-vm', '2012-11-24 23:59:59', 'dev.tsl.gr', '820', '147.52.82.46', '00:16:3E:65:34:1A', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('Nikolaou-vm', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('nms_60930_A', '2014-11-13 06:59:08', '2(SERVFAIL', '810', '147.52.12.231', '00:16:3E:17:01:CB', '255.255.255.240', '147.52.12.225', NULL, NULL, '4'),
('nms_60930_B', '2014-11-20 07:54:00', '2(SERVFAIL', '810', '147.52.12.232', '00:16:3E:4F:6F:70', '255.255.255.240', '147.52.12.225', NULL, NULL, '4'),
('nms_60930_E', '2014-11-25 10:58:43', '2(SERVFAIL', '810', '147.52.12.230', '00:16:3E:39:40:EB', '255.255.255.240', '147.52.12.225', NULL, NULL, '4'),
('old-cleano', '2015-06-02 18:37:55', '', '825', '147.52.82.184', '52:54:05:11:03:57', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('oldeschool-med', '2015-07-21 08:22:28', 'vml.med.uoc.gr', '820', '147.52.82.31', '52:54:00:D7:5A:3B', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('om_ucnet_60551', '2014-10-01 06:23:42', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('openeclass', '2012-11-24 23:59:59', 'telesforos.uoc.gr', '820', '147.52.82.66', '00:16:3E:32:00:5C', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('OpenSIPS_2.1_VM', '2015-07-27 09:58:52', 'sip.ucdc.uoc.gr', '825', '147.52.82.183', '00:16:3E:55:98:F7', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('OpenSIPS_2.1_VM', '2015-07-27 09:58:52', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('OTS_61366_app1', '2015-02-09 06:41:41', 'ots-31366-web1.datacenter.uoc.gr', '862', '147.52.200.88', '00:16:3E:46:AB:11', '255.255.255.240', '147.52.200.84', NULL, NULL, '4'),
('OTS_61366_app2', '2015-02-09 09:14:38', 'ots-31366-app2.datacenter.uoc.gr', '861', '147.52.200.72', '00:16:3E:62:77:DE', '255.255.255.240', '147.52.200.68', NULL, NULL, '4'),
('OTS_61366_db1', '2015-02-09 09:46:14', 'ots-31366-db1.datacenter.uoc.gr', '860', '147.52.200.56', '00:16:3E:53:90:04', '255.255.255.240', '147.52.200.52', NULL, NULL, '4'),
('OTS_62427_web2', '2015-09-18 10:05:58', 'ots-62427-web2.datacenter.uoc.gr', '862', '147.52.200.89', '00:16:3E:78:3C:91', '255.255.255.240', '147.52.200.84', NULL, NULL, '4'),
('owncloud', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('peir-gym-ira', '2012-11-24 23:59:59', '', '820', '147.52.82.56', '00:16:3E:7F:C7:DE', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('PeriferiaDCtun-Srv', '2017-09-18 07:20:40', '', '825', '147.52.82.146', '00:16:3E:79:16:22', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('PeriferiaDCtun-Srv', '2017-09-18 07:20:40', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('periferia_57530_app', '2013-05-17 05:38:51', '', '2561', '192.168.19.20', '00:16:3E:30:EF:CF', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_57530_app', '2013-05-17 05:38:51', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_57530_db', '2013-05-17 06:12:08', '', '2561', '192.168.19.21', '00:16:3E:6B:64:4C', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_57530_db', '2013-05-17 06:12:08', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_59070_ubuntu', '2014-02-27 09:48:15', 'periferia-59070.datacenter.uoc.gr', '820', '147.52.82.116', '00:16:3E:41:9D:21', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('periferia_64574_app', '2016-06-22 10:26:37', '', '2561', '192.168.19.40', '00:16:3E:26:31:01', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_64574_app', '2016-06-22 10:26:37', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_64574_db', '2016-06-22 10:29:29', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_65714_vhosting', '2016-12-06 11:08:53', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_admin1', '2017-07-04 05:51:41', '', '825', '147.52.82.145', '00:16:3E:3F:3A:AA', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('periferia_ekloges_admin1', '2017-07-04 05:51:41', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('periferia_ekloges_hadb', '2017-06-21 08:21:36', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_hapublic', '2017-07-03 06:06:38', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_mdb01', '2017-06-14 07:40:34', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_mdb02', '2017-06-14 07:49:53', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_mdb03', '2017-06-14 07:55:10', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_web1', '2017-07-03 06:31:46', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_ekloges_web2', '2017-07-04 05:15:03', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('periferia_papyros', '2015-10-09 06:55:20', NULL, '2561', 'NULL', 'NULL', '255.255.255.0', '147.52.19.254', NULL, NULL, '4'),
('Phl1_Reth', '2012-11-24 23:59:59', 'phlsrv.phl.uoc.gr', '820', '147.52.82.121', '00:16:3E:56:D7:B8', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('plex_57141', '2012-11-24 23:59:59', 'dp.datacenter.uoc.gr', '820', '147.52.82.70', '00:16:3E:23:A6:84', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('pop-imap', '2012-11-24 23:59:59', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('PTDE_66588', '2017-04-28 10:33:40', 'ptde-66588.datacenter.uoc.gr', '820', '147.52.82.51', '00:16:3E:35:65:71', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('PTDE_66588', '2017-04-28 10:33:40', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('radius1', '2014-03-20 07:06:03', 'radius.ucnet.uoc.gr', '15', '147.52.80.49', '00:16:3E:41:28:0F', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('radius1', '2014-03-20 07:06:03', 'radius1.ucnet.uoc.gr', '15', '147.52.80.50', '00:16:3E:41:28:0F', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('radius2', '2016-09-29 11:46:02', 'radius2.ucnet.uoc.gr', '15', '147.52.80.51', '00:16:3E:5C:6E:6A', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('seafile-srv', '2015-09-17 12:47:13', 'mydisk.datacenter.uoc.gr', '825', '147.52.82.175', '00:16:3E:32:D2:A0', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('seafile-srv', '2015-09-17 12:47:13', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('secr-db-1', '2015-05-13 13:54:31', '', '869', '147.52.200.233', '00:0C:29:4E:6E:A6', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-rports-1', '2015-05-14 08:32:36', '', '869', '147.52.200.237', '00:16:3E:12:0A:76', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-rports-1', '2015-05-14 08:32:36', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-1', '2015-05-13 13:44:58', '', '869', '147.52.200.236', '52:54:00:35:62:83', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-1', '2015-05-13 13:44:58', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-2', '2015-09-23 07:10:43', '', '869', '147.52.200.239', '00:16:3E:72:6A:14', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-2', '2015-09-23 07:10:43', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-3', '2015-09-23 07:18:35', '', '869', '147.52.200.240', '00:16:3E:57:3E:79', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-3', '2015-09-23 07:18:35', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-4', '2015-09-23 07:20:28', '', '869', '147.52.200.241', '00:16:3E:78:47:F4', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-4', '2015-09-23 07:20:28', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-5', '2015-09-23 08:07:03', '', '869', '147.52.200.242', '00:16:3E:0F:98:D0', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-5', '2015-09-23 08:07:03', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-6', '2015-09-23 08:08:57', '', '869', '147.52.200.243', '00:16:3E:2B:F6:86', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-6', '2015-09-23 08:08:57', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-7', '2015-09-23 08:16:20', '', '869', '147.52.200.244', '00:16:3E:49:8B:C2', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-7', '2015-09-23 08:16:20', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-lb', '2015-09-23 08:52:11', '', '869', '147.52.200.236', '00:16:3E:76:9A:04', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secr-web-lb', '2015-09-23 08:52:11', NULL, '869', 'NULL', 'NULL', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secrapp-class-1', '2012-11-24 23:59:59', '', '869', '147.52.200.232', '00:0C:29:40:E9:1C', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secrapp-class-1', '2012-11-24 23:59:59', '', '869', '147.52.200.238', '00:0C:29:40:E9:1C', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secrapp-stdnt-1', '2015-05-13 13:04:53', '', '869', '147.52.200.235', '00:1A:4A:34:9F:2A', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('secrweb-profs-1', '2015-05-14 08:30:10', '', '869', '147.52.200.234', '00:16:3E:4C:E5:6A', '255.255.255.224', '147.52.200.228', NULL, NULL, '4'),
('shibboleth', '2013-07-11 13:02:09', 'login.uoc.gr', '825', '147.52.82.205', '00:16:3E:46:54:24', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('sites-vits', '2012-11-24 23:59:59', 'sites.datacenter.uoc.gr', '820', '147.52.82.65', '00:16:3E:73:CB:14', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('soc-66915', '2017-06-29 08:41:48', 'soc-adserver.soc.uoc.gr', '825', '147.52.82.174', '00:16:3E:3F:57:CD', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('soc-66915', '2017-06-29 08:41:48', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('sso', '2012-11-24 23:59:59', 'sso.uoc.gr', '820', '147.52.82.61', '00:16:3E:49:61:72', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('stats-ucnet', '2014-07-28 06:04:25', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('stirizo-devel2', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('stirizo-devel2-PSD', '2012-11-24 23:59:59', 'video.ucnet.uoc.gr', '820', '147.52.82.48', '00:16:3E:2C:AF:B5', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('stirizo-devel2-PSD', '2012-11-24 23:59:59', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('stirizo-devel3', '2012-11-24 23:59:59', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('stirizo-devel3-PSD', '2013-08-05 13:44:51', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('synnefo-node1', '2016-10-17 11:00:59', 'synnefo.ucdc.uoc.gr', '825', '147.52.82.207', '00:16:3E:00:84:18', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('synnefo-node1', '2016-10-17 11:00:59', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('synnefo-node2', '2016-10-17 11:02:29', 'pithos.ucdc.uoc.gr', '825', '147.52.82.208', '00:16:3E:45:01:83', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('synnefo-node2', '2016-10-17 11:02:29', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('tacacs', '2015-11-20 10:49:11', '2(SERVFAIL', '810', '147.52.12.235', '00:16:3E:42:AD:59', '255.255.255.240', '147.52.12.225', NULL, NULL, '4'),
('telesforos', '2012-11-24 23:59:59', 'telesforos.datacenter.uoc.gr', '820', '147.52.82.59', '00:16:3E:41:6F:93', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('test-VM-jpat-32G', '2015-03-11 14:25:29', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('test_vlan15', '2013-08-01 12:31:06', NULL, '15', 'NULL', 'NULL', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('tety-64515', '2016-06-08 08:46:58', 'mail.materials.uoc.gr', '820', '147.52.82.120', '00:16:3E:49:DF:F1', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tety-s2', '2013-11-01 11:37:41', 'pandora.materials.uoc.gr', '820', '147.52.82.74', '00:16:3E:1E:60:8A', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tety-s3', '2014-06-16 09:24:00', 'minos.materials.uoc.gr', '820', '147.52.82.88', '00:16:3E:67:81:EC', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tetyserver1', '2012-11-24 23:59:59', 'chaos.materials.uoc.gr', '820', '147.52.82.45', '00:16:3E:45:A6:38', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tety_s4_64733', '2016-07-29 06:16:36', '', '820', '147.52.82.87', '00:16:3E:23:DD:A4', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('TicketID_60624', '2014-12-22 13:10:26', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64645', '2016-07-25 05:42:42', 'tid-64645.datacenter.uoc.gr', '820', '147.52.82.85', '00:16:3E:69:67:3D', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('TicketID_64645', '2016-07-25 05:42:42', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('TicketID_64770_1', '2016-08-09 07:53:07', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_2', '2016-08-09 08:06:50', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_2_m', '2016-08-09 09:41:46', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_3', '2016-08-09 08:25:46', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_3_m', '2016-08-09 09:39:02', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_4', '2016-08-10 20:35:15', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_4_m', '2016-08-10 20:38:29', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_5', '2016-08-10 20:44:59', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_5_m', '2016-08-10 20:46:41', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_6', '2016-08-10 21:02:25', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_64770_6_m', '2016-08-10 21:08:03', NULL, '880', 'NULL', 'NULL', '255.255.255.224', '147.52.204.3', NULL, NULL, '4'),
('TicketID_65409_lib', '2016-11-16 08:12:44', 'libserver6.datacenter.uoc.gr', '820', '147.52.82.29', '00:16:3E:77:BB:5F', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tmp-kaltura', '2013-10-01 08:45:34', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('tmp_59749', '2014-06-03 05:16:07', 'tmp-59749.datacenter.uoc.gr', '820', '147.52.82.89', '00:16:3E:5F:B5:4E', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tms-vc-int', '2013-11-19 10:56:16', '', '792', '10.250.64.131', '00:16:3E:75:66:CE', '255.255.255.0', '10.0.64.1', NULL, NULL, '4'),
('tolis_59192', '2014-03-17 07:03:04', 'tolis-59192.datacenter.uoc.gr', '820', '147.52.82.117', '00:16:3E:42:B1:EC', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tolis_59192', '2014-03-17 07:03:04', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('torps', '2014-04-02 05:52:49', 'torps.datacenter.uoc.gr', '820', '147.52.82.96', '00:16:3E:33:24:9A', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('tyserv', '2013-08-04 20:53:24', 'tyserv.tec.uoc.gr', '15', '147.52.80.33', '52:54:00:11:06:47', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('UCnetDCtun', '2015-05-26 20:25:00', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('ultivpn', '2018-01-16 09:05:52', 'ultivpn.uoc.gr', '824', '147.52.9.10', '00:16:3E:2C:96:EE', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vc-int', '2013-11-19 06:46:13', '', '792', '10.250.64.250', '00:16:3E:60:7B:3C', '255.255.255.0', '10.0.64.1', NULL, NULL, '4'),
('vc-int', '2013-11-19 06:46:13', '', '792', '10.250.64.251', '00:16:3E:60:7B:3C', '255.255.255.0', '10.0.64.1', NULL, NULL, '4'),
('vc-int', '2013-11-19 06:46:13', NULL, '792', 'NULL', 'NULL', '255.255.255.0', '10.0.64.1', NULL, NULL, '4'),
('VDi-1-prytaneia', '2014-01-20 09:43:24', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('velos', '2013-08-07 11:48:00', '', '15', '147.52.80.32', '00:16:3E:35:D6:95', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('vhyp1', '2015-01-26 09:06:00', NULL, '825', 'NULL', 'NULL', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('vod-telesforos', '2014-01-28 06:41:19', NULL, '820', 'NULL', 'NULL', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('vod-ucnet', '2014-07-24 06:52:19', '', '825', '147.52.82.197', '00:16:3E:59:C3:3E', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('vod-ucnet', '2014-07-24 06:52:19', '', '825', '147.52.82.198', '00:16:3E:59:C3:3E', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('voip-proxy', '2013-08-02 09:01:08', 'voip-proxy.voip.uoc.gr', '15', '147.52.80.5', '00:16:3E:83:9E:F6', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('voip-proxy', '2013-08-02 09:01:08', NULL, '15', 'NULL', 'NULL', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('VoipMgmtsrv', '2012-11-24 23:59:59', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('VoipServer-NNI1', '2015-02-13 09:31:19', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer2', '2012-11-24 23:59:59', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('VoipServer3', '2012-11-24 23:59:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer4', '2012-11-24 23:59:59', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('VoipServer5-SBC1', '2014-06-26 10:03:09', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer6-X11', '2012-11-24 23:59:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer7', '2012-11-24 23:59:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer8', '2012-11-24 23:59:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer8_112', '2012-11-24 23:59:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer_fserv', '2016-08-31 11:43:49', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('VoipServer_Hylafax_Server', '2015-09-24 09:41:33', NULL, '702', 'NULL', 'NULL', '255.255.255.240', '194.63.237.225', NULL, NULL, '4'),
('VoipServer_MariaDB_node1', '2015-08-19 07:10:39', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('VoipServer_MariaDB_node2', '2015-08-19 07:13:59', NULL, '701', 'NULL', 'NULL', '255.255.255.0', '10.0.78.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'vpnserver2.ucnet.uoc.gr', '824', '147.52.9.2', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-50.wifi.uoc.gr', '824', '147.52.9.50', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-51.wifi.uoc.gr', '824', '147.52.9.51', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-52.wifi.uoc.gr', '824', '147.52.9.52', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-53.wifi.uoc.gr', '824', '147.52.9.53', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-54.wifi.uoc.gr', '824', '147.52.9.54', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-55.wifi.uoc.gr', '824', '147.52.9.55', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-56.wifi.uoc.gr', '824', '147.52.9.56', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-57.wifi.uoc.gr', '824', '147.52.9.57', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-58.wifi.uoc.gr', '824', '147.52.9.58', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-59.wifi.uoc.gr', '824', '147.52.9.59', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-60.wifi.uoc.gr', '824', '147.52.9.60', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-61.wifi.uoc.gr', '824', '147.52.9.61', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-62.wifi.uoc.gr', '824', '147.52.9.62', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-63.wifi.uoc.gr', '824', '147.52.9.63', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-64.wifi.uoc.gr', '824', '147.52.9.64', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-65.wifi.uoc.gr', '824', '147.52.9.65', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-66.wifi.uoc.gr', '824', '147.52.9.66', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-67.wifi.uoc.gr', '824', '147.52.9.67', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-68.wifi.uoc.gr', '824', '147.52.9.68', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-69.wifi.uoc.gr', '824', '147.52.9.69', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-70.wifi.uoc.gr', '824', '147.52.9.70', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-71.wifi.uoc.gr', '824', '147.52.9.71', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-72.wifi.uoc.gr', '824', '147.52.9.72', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-73.wifi.uoc.gr', '824', '147.52.9.73', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-74.wifi.uoc.gr', '824', '147.52.9.74', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-75.wifi.uoc.gr', '824', '147.52.9.75', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-76.wifi.uoc.gr', '824', '147.52.9.76', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', 'client-9-77.wifi.uoc.gr', '824', '147.52.9.77', '00:16:3E:63:9A:A1', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('vpnserver2', '2013-11-20 10:30:25', NULL, '824', 'NULL', 'NULL', '255.255.255.0', '147.52.9.1', NULL, NULL, '4'),
('wcs-manager', '2013-08-02 07:46:05', NULL, '15', 'NULL', 'NULL', '255.255.255.192', '147.52.80.62', NULL, NULL, '4'),
('web', '2012-11-24 23:59:59', NULL, '3000', 'NULL', 'NULL', '255.255.255.0', '10.1.1.1', NULL, NULL, '4'),
('Win7_THIN_DEMO', '2013-05-21 05:38:24', '', '820', '147.52.82.36', '00:16:3E:38:EE:B5', '255.255.255.128', '147.52.82.126', NULL, NULL, '4'),
('wowza-ucnet', '2014-08-04 06:56:46', 'wowza.ucnet.uoc.gr', '825', '147.52.82.169', '00:16:3E:2A:30:8B', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('www_ucnet', '2014-06-23 06:55:10', 'ucnet.ucdc.uoc.gr', '825', '147.52.82.187', '00:16:3E:03:84:66', '255.255.255.128', '147.52.82.254', NULL, NULL, '4'),
('xalkis-61656-fs', '2015-04-23 09:18:08', '', '850', '147.52.200.7', '00:16:3E:0E:48:91', '255.255.255.240', '147.52.200.3', NULL, NULL, '4'),
('xalkis-62930-Win7', '2015-10-26 10:07:06', '', '862', '147.52.200.94', '00:16:3E:7D:41:01', '255.255.255.240', '147.52.200.84', NULL, NULL, '4'),
('xalkis-65677', '2016-12-14 10:06:12', '', '860', '147.52.200.57', '00:16:3E:42:9F:CB', '255.255.255.240', '147.52.200.52', NULL, NULL, '4'),
('xalkis-66797', '2017-05-29 12:20:07', '', '855', '147.52.200.38', '00:16:3E:3B:02:32', '255.255.255.240', '147.52.200.35', NULL, NULL, '4'),
('xalkis-66797', '2017-05-29 12:20:07', '', '861', '147.52.200.74', '00:16:3E:3B:02:32', '255.255.255.240', '147.52.200.68', NULL, NULL, '4'),
('xalkis-66910', '2017-06-26 11:35:46', '', '860', '147.52.200.58', '00:16:3E:1D:E9:F5', '255.255.255.240', '147.52.200.52', NULL, NULL, '4'),
('xalkis-66910', '2017-06-26 11:35:46', NULL, '860', 'NULL', 'NULL', '255.255.255.240', '147.52.200.52', NULL, NULL, '4'),
('xenios', '2013-02-22 10:48:06', 'xenios.datacenter.uoc.gr', '820', '147.52.82.123', '00:16:3E:36:50:BE', '255.255.255.128', '147.52.82.126', NULL, NULL, '4');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `vmOwners`
--

CREATE TABLE `vmOwners` (
  `vmName` varchar(64) NOT NULL,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OwnerID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `vmTechnicians`
--

CREATE TABLE `vmTechnicians` (
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TechnicianID` int(11) NOT NULL,
  `vmName` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `Carriers`
--
ALTER TABLE `Carriers`
  ADD PRIMARY KEY (`CarrierName`,`School`,`Department`,`Sector`);

--
-- Ευρετήρια για πίνακα `IaaS`
--
ALTER TABLE `IaaS`
  ADD PRIMARY KEY (`vmName`);

--
-- Ευρετήρια για πίνακα `Owners`
--
ALTER TABLE `Owners`
  ADD PRIMARY KEY (`ID`,`ownerName`,`ownerSurname`);

--
-- Ευρετήρια για πίνακα `PersonInCharge`
--
ALTER TABLE `PersonInCharge`
  ADD PRIMARY KEY (`ID`,`Name`,`Surname`,`PhoneNum`);

--
-- Ευρετήρια για πίνακα `Technicians`
--
ALTER TABLE `Technicians`
  ADD PRIMARY KEY (`ID`,`techName`,`techSurname`);

--
-- Ευρετήρια για πίνακα `vmCharacteristics`
--
ALTER TABLE `vmCharacteristics`
  ADD PRIMARY KEY (`vmName`);

--
-- Ευρετήρια για πίνακα `vmNetwork`
--
ALTER TABLE `vmNetwork`
  ADD PRIMARY KEY (`vmName`,`MAC`,`IPAddress`);

--
-- Ευρετήρια για πίνακα `vmOwners`
--
ALTER TABLE `vmOwners`
  ADD PRIMARY KEY (`OwnerID`,`vmName`);

--
-- Ευρετήρια για πίνακα `vmTechnicians`
--
ALTER TABLE `vmTechnicians`
  ADD PRIMARY KEY (`TechnicianID`,`vmName`);

--
-- AUTO_INCREMENT για άχρηστους πίνακες
--

--
-- AUTO_INCREMENT για πίνακα `Owners`
--
ALTER TABLE `Owners`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `PersonInCharge`
--
ALTER TABLE `PersonInCharge`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT για πίνακα `Technicians`
--
ALTER TABLE `Technicians`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
