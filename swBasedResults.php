<?php

/*db is accessed to retrieve all applications*/
require("page.php");

  $resultsPage = new Page();	
  
  $resultsPage -> DisplayUpper();
 
  $resultsPage -> resultsTitle = "<p id='res'>Αποτελέσματα</p>";
  $resultsPage -> DisplayContent2();

  if (empty($_POST["swBasedInput"])) {
			echo "<script src='js/alert_if_empty.js'></script>";
  }
  $inputSw = $_POST["swBasedInput"];
  
  /* access DB to retrieve applications*/	
  $link = mysqli_connect('localhost', 'ucdcuser', 'ucdc123', 'ucdc');

  //for greek characters to be recognized
  mysqli_set_charset($link, "utf8");

  $query = "SELECT SwUrl, Description, SwBased, SwLanguage, 
                   DbType, ownerName, ownerSurname, ServerName
            FROM Applications, Owners
            WHERE LOWER(SwBased) like LOWER('%$inputSw%')
            AND Applications.OwnerID = Owners.ID";

$k = 0; //counter for odd table rows
if($result = mysqli_query($link, $query)){
  if(mysqli_num_rows($result) > 0){
    echo "<table>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Software Url</th>";
    echo "<th class='desRow'>Description</th>";
    echo "<th>Software Based</th>";
    echo "<th>Software Language</th>";
    echo "<th>Database Type</th>";
    echo "<th>Owner Name</th>";
    echo "<th>Owner Surname</th>";
    echo "<th>Server Name</th>";
    echo "</tr>";
    echo "</thead>";
    while($row = mysqli_fetch_array($result)){
      echo "<tbody>";  
      if ($k%2 == 0){
        echo "<tr class = 'odd'>";
        echo "<td>$row[0]</td>";
        echo "<td class='desRow'>$row[1]</td>";
        echo "<td>$row[2]</td>";
        echo "<td>$row[3]</td>";
        echo "<td>$row[4]</td>";
        echo "<td>$row[5]</td>";
        echo "<td>$row[6]</td>";
        echo "<td>$row[7]</td>";
        echo "</tr>";
      } else {
        echo "<tr>";
        echo "<td>$row[0]</td>";
        echo "<td class='desRow'>$row[1]</td>";
        echo "<td>$row[2]</td>";
        echo "<td>$row[3]</td>";
        echo "<td>$row[4]</td>";
        echo "<td>$row[5]</td>";
        echo "<td>$row[6]</td>";
        echo "<td>$row[7]</td>";
        echo "</tr>";
      }
    $k++;
    echo "</tbody>";
    }
    echo "</table>";

    mysqli_free_result($result);
  } else{
    echo "Δε βρέθηκαν εγγραφές.";
  }
} else{
  echo "ERROR: Could not able to execute $query. " . mysqli_error($link);
}          

  $resultsPage -> DisplayBottom();
