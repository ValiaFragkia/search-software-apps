CREATE TABLE vmCharacteristics (
  vmName varchar(254) NOT NULL,
  firstTicketID int(6) DEFAULT NULL,
  creationDate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modificationDate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  modificationTicketID int(6) NOT NULL DEFAULT '0',
  vCores enum('1','2','4','6','8','10','12','16','24') NOT NULL DEFAULT '2',
  vRam enum('0.5','1','2','4','6','8','10','12','14','16','18','20','22','24','28','32') NOT NULL DEFAULT '2',
  vNICs enum('0','1','2','3','4','5','6') DEFAULT '1',
  vHDDs enum('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15') DEFAULT '1',
  totalvHDDSizeInGB decimal(10,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;