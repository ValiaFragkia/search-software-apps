<?php
class Page
{
	public $errorMessage;
	public $resultsTitle;
	public $results;
	public $btn;
	public $title = "UOC Software Registering Tool";
	public $form;
	
	 // class Page's operations
	public function __set($name, $value)
	{
		$this->$name = $value;
	}
  
	public function DisplayUpper()
    {
		echo "<html>\n<head>\n";
		$this -> DisplayTitle();
		$this -> DisplayStyles();
		echo "</head>\n<body>\n";
		$this -> DisplayHeader();
	}

	public function DisplayContent()
		{
		$this -> DisplayForm();
		echo $this -> errorMessage;
		echo $this->resultsTitle;
		echo $this->results;
		echo $this->btn;
		}
		
	public function DisplayContent2()
		{
		$this -> DisplayForm2();
		echo $this -> errorMessage;
		echo $this->resultsTitle;
		echo $this->results;
		echo $this->btn;
		}

	public function DisplayBottom()
    {
		$this -> DisplayFooter();
		echo "</body>\n</html>\n";
	}
		
	public function DisplayTitle()
	{
		echo "<title>".$this->title."</title>";
	}

	public function DisplayStyles()
	{	 
		?>   
		<link href="css/styles.css" type="text/css" rel="stylesheet">
		<?php
	}

	public function DisplayHeader()
	{ 
		?>   
		<section id="page">
			<!-- page header -->
			<header>
				<meta charset="utf-8">
				<img id="image-align-left" src="uoc_logo.png" alt="uoc logo" height="90" width="90" align = "left"/> 
				<h2>ΠΑΝΕΠΙΣΤΗΜΙΟ ΚΡΗΤΗΣ</h2>
				<img id="image-align-right" src="ucnet_logo.png" alt="uoc logo" height="100" width="240"/> 
			</header><!-- end header -->
		<?php
	}
	
	public function DisplayForm()
	{
		?>
		<section id="content">
			<form action="resultsRedirect.php" method="post">
				<p id='choice'><strong>Επιλέξτε προς Αναζήτηση:</strong></p><br />
					<select id="selection" name="searchtype">
						<option value="allApps">Εμφάνιση όλων των εφαρμογών</option>
						<option value="appsServer">Ποιες εφαρμογές ανήκουν σε ένα server</option>
						<option value="swBased">Ποες εφαρογμές βασίζονται σε ένα λογισμικό</option>
					</select>
					<br />
				<input id='first-button' type="submit" name="submit" value="Search">
			</form>
		<?php
	}

	public function DisplayForm2()
	{
		?>
		<section id="content">
			<form action="swBasedResults.php" method="post">
				<fieldset>
					<legend>Επιλέξτε προς Αναζήτηση:</legend>
						<label></label>
						<input type='text' name = 'swBasedInput' placeholder ='όνομα λογισμικού'>
				</fieldset>
						<input type="submit" name="submit" value="Search">
			</form>
		<?php
	}
	
	public function DisplayFooter()
	{
		?>
			</section>
		</section>
		<footer>
			<div id="copyright-align-left">
				<p>© Copyright 2018 ucnet.uoc.gr</p>
			</div>
			<div id="copyright-align-right">
				<a href="http://www.uoc.gr/" target="_blank"><img alt="uoc logo small" src="uoc_logo_small.png" width="70" height="70"></a>
			</div>
		</footer>
		<?php
	}
}
?>