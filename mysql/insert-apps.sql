INSERT INTO Applications(SwUrl,Description,SwBased,SwLanguage,DbType,OwnerID, ServerName) VALUES
 ('http://booking.ucdc.uoc.gr/','Σύστημα Κρατήσεων για τα ακόλουθα:
- Αιθουσών UCnet 
- Κεντρικής Διατμηματικής Αίθουσας τηλε-εκπαιδευσης E-130 Κτ. Μαθ. 
- Κεντρικών Συστημάτων Τηλεδιάσκεψης
- Εργαστήριο Βίντεο (Video Lab)
- Κρατήσεις Δικτυακών Καμερών','phpscheduleit', 'php', 'mysql',1,'cleano'),
 ('http://delos.datacenter.uoc.gr/opendelos/','Συγχρονισμός βίντεο – διαφανειών, κράτηση αιθουσών και προγραμματισμός IP καμερών',
 'λογισμικό OS, φτιαγμένο στα πλαίσια του προγράμματος Opencourses από το ΕΚΠΑ και άλλους φορείς',
  'php', '',1,'cleano3'),
 ('https://helpdesk.ucnet.uoc.gr/','helpdesk Πανεπιστημίου','custom', 'php', 'mysql',2,'cleano'),
 ('http://www.ucnet.uoc.gr/','Επίσημη ιστοσελίδα ΚΕΔ','drupal', 'php', 'mysql',1,'cleano'),
 ('http://classroom-booking.uoc.gr/ ','Σύστημα Κρατήσεων αιθουσών Τμημάτων ΤΕΥ και ΜΑΘ',
 'mrbs', 'php', 'mysql',1,'cleano3'),
 ('http://mrbs-pryt.ucnet.uoc.gr/','Σύστημα Κρατήσεων αιθουσών Διοίκησης Βούτες και Γάλλου',
 'mrbs', 'php', 'mysql',1,'cleano3'),
 ('http://cict.uoc.gr/','Ιστοσελίδα για το ΚΤΠΕ','wordpress', 'php', '??? Χρησιμοποιείται 
Την διαχειρίζονται άλλοι (Βιτσάκης)',1,'cleano3'),
 ('biotalent.ucdc.uoc.gr','Σύστημα e-μάθησης για το έργο BIOTALENT',
 'moodle', 'php', 'mysql',1,'biotalent-65807'),
 ('old-www.med.uoc.gr','Παλιό site Ιατρικής','', 'php', 'mysql',5,'med-hydra'),
 ('biochemistry.med.uoc.gr','Τμήμα Βιοχημείας Ιατρική – πολύ παλιό','phpnuke', 'php', 'mysql',6,'med-hydra'),
 ('cbt.med.uoc.gr','Κλινική Ψυχιατρική','phpCake', 'php', 'mysql',7,'med-hydra')
 
 